<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Generate Sanction Letter</name>
   <tag></tag>
   <elementGuidId>dcb1e818-d221-4999-9724-c9ef171f0257</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/button/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Generate Sanction Letter</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;app__AppWrapper-iDfyMj dEEurA&quot;]/div[1]/div[@class=&quot;app-layout app-view&quot;]/div[@class=&quot;AppWrapperStyles__AppContentWrapper-dYymKV jKJxMu&quot;]/div[@class=&quot;DashboardStyles__DashboardWrapper-egdBYD bXSbVK&quot;]/div[@class=&quot;DashboardStyles__JourneyWrapper-HJJnM ckrqdk&quot;]/div[3]/div[1]/div[@class=&quot;CardStyles__CardContainer-eHPirp jgGPgu&quot;]/div[@class=&quot;CardStyles__ButtonWrapperStyle-jBhMgJ dpMMYr&quot;]/div[2]/button[1]/div[1]/div[1]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/button/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send Sanction Terms'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saved Sanction Letter'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments :'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div[2]/button/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
//WebUI.openBrowser(var_url)
WebUI.openBrowser('http://dev-re.svakarma.net')

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Email Id_ku-text-e'), var_email)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Email Id_ku-text-e'), 
    'relationship.manager.1@dev.com')

//Enter Password
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Password_ku-text-p'), var_password)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Password_ku-text-p'), 
    'relationship.manager.1')

//Select CheckBox 
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Password_ku-checkb'))

//Click on Submit to Login
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/span_Submit'))

WebUI.delay(2)

//Click to Select the Lead
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectLead/span_SelectLead'))
String Id = "$dynamicId"

String xpath = ('//span[contains(text(), "' + Id) + '" )]'

TestObject to = new TestObject('path')

to.addProperty('xpath', ConditionType.EQUALS, xpath)

WebUI.click(to)

WebUI.delay(1)

//Click on Loan Application Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/span_Loan Application Details'))

//Click on Select Repayment Frequency
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/divRepayfrq'))

WebUI.delay(2)

//Click on EMI option
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/div_Repayment'))

WebUI.delay(2)

//Click on Select Security offered
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/divSecurityOffered'))

WebUI.delay(2)

//Click on option 
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/div_Automobile'))

WebUI.delay(2)

//Click on Select ID Proof of the Enterprise
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/divIDProof'))

WebUI.delay(2)

//Click on option PAN
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/div_PAN'))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/span_Loan Application Details'))

WebUI.delay(1)

//Click on Business Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/span_Business Details'))

WebUI.delay(2)

//Click on Business Vintage
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/input__ku-text-businessVintage'), var_businessVintage)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/input__ku-text-businessVintage'), '10')

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/span_Business Details'))

WebUI.delay(1)

//Click on Personal Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/span_Personal Details'))

WebUI.delay(1)

//Send DoB
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/input__ku-datepicker-dob'))

WebUI.delay(2)

//Calender Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/Tab_Calender/div_2019'))

WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/Tab_Calender/span_1973'))

WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/Tab_Calender/span_27'))

WebUI.delay(2)

//Select Gender
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/divGender'))

WebUI.delay(1)

//Select as
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/div_SelectGender'))

WebUI.delay(1)

//Choose Marital Status
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/divMaritalStatus'))

WebUI.delay(1)

//Select as
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/div_SelectStatus'))

WebUI.delay(1)

//Choose Qualification
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/divEducation'))

WebUI.delay(1)

//Select as
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/div_SelectEducation'))

WebUI.delay(1)

//Pass the Languages known
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/input__ku-text-languagesKnown'), 'English')

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/span_Personal Details'))

WebUI.delay(1)

//Open ID Proof tab
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/span_ID-Proof'))

//Enter PAN Card Number
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input__ku-text-panApplicant'), var_PANnumber)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input__ku-text-panApplicant'), 'AAAPL1235C')

//Enter Aadhar Number
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input__ku-text-aadharNumber'), var_Aadharnumber)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input__ku-text-aadharNumber'), '499118665248')

WebUI.delay(1)

//Driving Licence ?
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input_Yes_radio-group-undefine'))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/span_ID-Proof'))

WebUI.delay(1)

//Open OwnerShip Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/span_Address and Ownership Det'))

//Enter the Residence Address 
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/input__ku-text-rAddressLine1'), '#28 Some Where on Earth')

//Enter the Pincode
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/input_Please enter a Pincode f'), '400022')

//Check the CheckBox
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/input_State_ku-checkbox-curren'))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/span_Address and Ownership Det'))

WebUI.delay(1)

//Open Bank Details 
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/span_Banking Details'))

//Enter the Account Number
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_Bank Account Number_ku-t'), '43000011111141192')

//Enter the IFSC code 
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_IFSC Code Branch Address'), 'SBIN0001372')

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_IFSC Code Branch Address'))

WebUI.delay(2)

//Enter the Vintage of Bank Acccount
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_Vintage of the Bank Acco'), '10')

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/span_Banking Details'))

WebUI.delay(1)

//Open Other Loan Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/span_Other Loan Details'))

//Any Other Loan
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input__radio-group-undefined'))

WebUI.delay(1)

//Enter the Name of Lender
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/textarea_Obligations (Current'), 
    'Some')

//Select Type of Loan
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/divLoanType'))

WebUI.delay(1)

//Choose as 
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/div_SelectLoanType'))

WebUI.delay(1)

//Select Collateral
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/divCollateral'))

WebUI.delay(1)

//Choose as
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/div_SelectCollateral'))

WebUI.delay(1)

//Amount of Loan Sanctioned
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_Obligations (Current Yea'), 
    '100000')

//Loan Distributed
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_hhjh_ku-text-loanDisburs'), 
    '100000')

//Rate of Inst
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_hhjh_ku-text-rateOfInter'), 
    '10')

//Principal o/s
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_hhjh_ku-text-principalOs'), 
    '1000')

//Original Tenor
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-originalTeno'), 
    '6')

//No.of Instalments
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-instalmentsP'), 
    '10')

//Balance Tenor
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-balanceTenor'), 
    '0')

//Monthly Obligation
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-monthlyOblig'), 
    '1')

//Obligations Current year
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-obligationsC'), 
    '1')

//Comment
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/ComntAndNext/textarea_Personal Feedback or'), 'Whatever you want')

//Process Next
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/ComntAndNext/span_Next'))

WebUI.delay(3)

//Proceed to PD1
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_ProceedToPD1/Page_Svakarma Finance/span_Next'))

WebUI.delay(3)

//Select PD1 Location
/*WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/div_PD1Location'))

WebUI.delay(2)

//Choose the Location
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/div_Business'))

WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/span_Next'))

WebUI.delay(3)*/
//Close Browser
WebUI.closeBrowser()


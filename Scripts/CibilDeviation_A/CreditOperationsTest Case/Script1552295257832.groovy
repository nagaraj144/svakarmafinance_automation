import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser("http://dev-bo.svakarma.net/auth/login")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('Object Repository/CreditOperationsManager/Page_creditoperationsLogin/input_Enter User Name..._ku-te'), "credit.operations.officer.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('Object Repository/CreditOperationsManager/Page_creditoperationsLogin/input_Enter Password..._ku-tex'), "credit.operations.officer.1")

//Click on Submit to Login
WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_creditoperationsLogin/span_Sign in'))
WebUI.delay(3)

//Select the Leads
//WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_SelectLead/td_SelectLead'))

String Id = "${dynamicId}"
String xpath = '//td[contains(text(), "' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(2)

//Select Sanction Letter
WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_SanctionLetter/li_Sanction letter'))
WebUI.delay(2)

//Edit
WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_SanctionLetter/span_EDIT'))

//Generate Sanction Letter
WebUI.scrollToElement(findTestObject('Object Repository/CreditOperationsManager/Page_SanctionandApprove/span_Generate Sanction Letter'), 2)
WebUI.delay(2)

//Click on Generate Saanction Letter
WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_SanctionandApprove/span_Generate Sanction Letter'))
WebUI.delay(8)

//Switching the tab
WebUI.switchToWindowIndex(0)
WebUI.delay(5)

//Approve
WebUI.scrollToElement(findTestObject('Object Repository/CreditOperationsManager/Page_SanctionLetter/span_Approve'), 2)
WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_SanctionLetter/span_Approve'))
WebUI.delay(2)

//Enter Comment
WebUI.sendKeys(findTestObject('Object Repository/CreditOperationsManager/Page_CommentAndSubmit/textarea_Enter Your Comment...'), "Proceed")
WebUI.delay(1)

//Submit
WebUI.click(findTestObject('Object Repository/CreditOperationsManager/Page_CommentAndSubmit/span_Submit'))
WebUI.delay(5)

//Close Browser
WebUI.closeBrowser()






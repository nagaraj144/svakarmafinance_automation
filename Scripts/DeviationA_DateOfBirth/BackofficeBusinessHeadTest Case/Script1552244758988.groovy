import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser("http://dev-bo.svakarma.net/auth/login")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('BackofficeBusinessHead/Page_BackOfficeBusinessHeadLogin/input_Enter User Name..._ku-te'), "business.head.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('BackofficeBusinessHead/Page_BackOfficeBusinessHeadLogin/input_Enter Password..._ku-tex'), "business.head.1")

//Click on Submit to Login
WebUI.click(findTestObject('BackofficeBusinessHead/Page_BackOfficeBusinessHeadLogin/span_Sign in'))
WebUI.delay(3)

//Select Lead
//WebUI.click(findTestObject('Object Repository/BackofficeBusinessHead/Page_SelectLead/td_SelectLead'))

String Id = "${dynamicId}"
String xpath = '//td[contains(text(), "' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(2)

//Approve
WebUI.click(findTestObject('Object Repository/BackofficeBusinessHead/Page_ApproveandSubmit/span_Approve'))

//Enter comments
WebUI.sendKeys(findTestObject('Object Repository/BackofficeBusinessHead/Page_ApproveandSubmit/textarea_Enter Your Comment...'), "Proceed")

//Submit
WebUI.click(findTestObject('Object Repository/BackofficeBusinessHead/Page_ApproveandSubmit/span_Submit'))
WebUI.delay(5)

//Close Browser
WebUI.closeBrowser()












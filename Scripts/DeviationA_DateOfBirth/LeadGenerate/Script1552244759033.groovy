import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType




//Launching Browser
WebUI.openBrowser(null)

//Maximizing the browser
WebUI.maximizeWindow(null)

'Login'

//Navigation to dev-re.svakarma.net
WebUI.navigateToUrl('http://dev-re.svakarma.net/')

//Passing User Name
//WebUI.sendKeys(findTestObject('Login/input_Enter Email Id_ku-text-e'), UserName)
WebUI.setText(findTestObject('Login/input_Enter Email Id_ku-text-e'), findTestData('Data Files/Credentials/Cred_Data').getValue(
		1, 1))

//Passing Password
//WebUI.sendKeys(findTestObject('Login/input_Enter Password_ku-text-p'), Password)
WebUI.sendKeys(findTestObject('Login/input_Enter Password_ku-text-p'), findTestData('Data Files/Credentials/Cred_Data').getValue(
		2, 1))

//Checkbox selection
WebUI.click(findTestObject('Login/input_Enter Password_ku-checkb'))

//Click on Login
WebUI.click(findTestObject('Login/span_Submit'))

WebUI.delay(2)

for(def rowNum=1; rowNum <=findTestData('Data Files/LeadGeneration/LeadGen_Data').getRowNumbers(); rowNum++)
{

'Creation of Lead'

//Click on New Lead Button
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/span_New Lead'))

/*String Id = "${dynamicId}"
String xpath = '//span[contains(text(),"' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
*/
Thread.sleep(2)

'Loan Application Details'

//Click on LoanApplicationDetails_Tab
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/LoanApplicationDetails_Tab'))

//Click on PurposeOfLoan_Dropdown
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/PurposeOfLoan_Dropdown'))

WebUI.delay(2)

//Selecting value from PurposeOfLoan Dropdown 
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/PuposeOfLoan_Working Capital'))

//WebUI.selectOptionByValue(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/PuposeOfLoan_Working Capital'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(1, 1))
not_run: WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/AmountOfLoanRequired'))

WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

//Click on AmountOfLoanRequired Text field
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/AmountOfLoanRequired'))

WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/AmountOfLoanRequired'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(3)

WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/AmountOfLoanRequired'), Keys.chord(Keys.BACK_SPACE))

//Passing the value to AmountOfLoanRequired from Excel file
//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/AmountOfLoanRequired'), '45000.00')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/AmountOfLoanRequired'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(1, rowNum))

WebUI.delay(2)

//Click on Tenure Text field
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/Tenure'))

WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/Tenure'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/Tenure'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(1)

//Passing the value to Tenure from Excel file

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/Tenure'), '8')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/Tenure'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(2, rowNum))

WebUI.delay(2)

//Click on RateOfInterest Text field

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/RateOfInterest'))

WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/RateOfInterest'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/RateOfInterest'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(1)

//Passing the value to RateOfInterest from Excel file

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/RateOfInterest'), '18')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/RateOfInterest'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		3, rowNum))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_LoanAppicationDetails/LoanApplicationDetails_Tab'))

WebUI.delay(2)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Business Details_Tab'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(1)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Enterprice_Name'))

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Enterprice_Name'), 'DemoLog')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Enterprice_Name'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		4, rowNum))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/NatureOfBusiness_Dropdown'))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Business_NameManufacturing'))

WebUI.delay(3)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/TypeOfEnterprice_Dropdown'))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Enterprices_PvtLtd'))

WebUI.delay(5)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Office_Address'))

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Office_Address'), 'Maharashtra')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Office_Address'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		5, rowNum))

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Office_Pincode'), '400022')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Office_Pincode'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		6, rowNum))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Annual_Turnover'))

WebUI.delay(1)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Annual_Turnover'))

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Annual_Turnover'), '150000')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Annual_Turnover'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		7, rowNum))

WebUI.delay(2)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Cluster'))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Cluster_Type'))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_BusinessDetails/Business Details_Tab'))

WebUI.delay(2)

'Personal Details'
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/Personal Details_Tab'))

not_run: WebUI.click(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/FirstName'))

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/FirstName'), 'RAjRAvi')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/FirstName'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		8, rowNum))

WebUI.delay(1)

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/LastName'), 'M')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/LastName'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		9, rowNum))

WebUI.delay(1)

//WebUI.sendKeys(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/Mobile_Number'), '9449639768')
WebUI.setText(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/Mobile_Number'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(
		10, rowNum))

WebUI.delay(1)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_PersonalDetails/Personal Details_Tab'))

WebUI.delay(2)

'Other Information'
WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/Other Information_Tab'))

WebUI.delay(2)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/Source_Dropdown'))

WebUI.delay(2)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/Page_Svakarma Finance/div_BC'))

WebUI.delay(5)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/div'))

WebUI.delay(2)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/div_MAH-NAGPUR'))

WebUI.delay(2)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/Other Information_Tab'))

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/span_Next'))

WebUI.delay(1)

WebUI.click(findTestObject('LeadGenerate/LeadGeneration_OtherInformations/Page_Svakarma Finance/ConfiamationMessage_Next'))

WebUI.delay(3)

}


WebUI.closeBrowser()



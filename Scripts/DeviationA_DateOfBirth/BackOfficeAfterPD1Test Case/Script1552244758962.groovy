import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser("http://dev-bo.svakarma.net/auth/login")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/input_Enter User Name..._ku-te'), "credit.rm.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/input_Enter Password..._ku-tex'), "credit.rm.1")

//Click on Submit to Login
WebUI.click(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/span_Sign in'))
WebUI.delay(1)

//Click to Select the Lead
//WebUI.click(findTestObject('BackOfficeAfterPD1/Page_SelectLead/td_SelectLead'))

String Id = "${dynamicId}"
String xpath = '//td[contains(text(),"' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(2)

//Select Bureau Details
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/li_Bureau Details'))
WebUI.delay(1)

//Edit to Enter CibilScore
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/span_EDIT'))

//Enter CibilScore
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_CIBIL Score - Applicant_'), "700")
WebUI.delay(2)

//Fill the CheckBoxes
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_concat(Note Borrowers'))
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_Dedupe Check_ku-checkbox'))
WebUI.delay(1)

//Save all the Details Edited
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/span_SAVE'))
WebUI.delay(8)

//Approve the Details
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/span_Approve'))
WebUI.delay(3)

//Enter the comments
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_CommentAndSubmit/textarea_Enter Your Comment...'), "Procced")

//Submit
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_CommentAndSubmit/span_Submit'))
WebUI.delay(3)

/*WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_CommentAndSubmit/span_Close'))
WebUI.delay(3)*/

//Close browser
WebUI.closeBrowser()









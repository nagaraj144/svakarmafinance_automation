import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Licenses
//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/ShopsAndEsta'))
String xpath33 = "(//input[@name='radio-group-shopEstablishmentActCertificate'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(49,1))+ "']"
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath33)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Gst_Cert'))
String xpath34 = "(//input[@name='radio-group-gstCertificate'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(50,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath34)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/ExportImport_Cert'))
String xpath35 = "(//input[@name='radio-group-exportImportCertificate'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(51,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath35)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/UdhyogAdhar'))
String xpath36 = "(//input[@name='radio-group-udyogAadhar'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(52,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath36)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/FoodSafety_Licence'))
String xpath37 = "(//input[@name='radio-group-foodSafetyLicense'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(53,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath37)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/FDA'))
String xpath38 = "(//input[@name='radio-group-fda'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(54,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath38)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/PollutionControl'))
String xpath39 = "(//input[@name='radio-group-pollutionControl'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(55,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath39)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Fire_Control'))
String xpath40 = "(//input[@name='radio-group-fireControl'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(56,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath40)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/FactriesAct'))
String xpath41 = "(//input[@name='radio-group-factoriesActCertificate'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(57,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath41)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/LicenceCheck'))
String xpath42 = "(//input[@name='radio-group-licenseCheck'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(58,1))+ "']"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath42)
WebUI.click(to)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Business_Finance_Next'))

WebUI.delay(3)

//Business Fianance
//Finance Working Capital
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Debtors_cycle'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Debtors_cycle'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Debtors_cycle'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(59,1))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Ceditors_cycle'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Ceditors_cycle'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Ceditors_cycle'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(60,1))

WebUI.click(findTestObject('PD1/Business Finance/Finance-Working Capital/Next'))

WebUI.delay(1)

//Financials Revenue
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/turnoverAsPerBooks'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/turnoverAsPerBooks'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/turnoverAsPerBooks'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(61,1))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Turnovers'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/operatingMarginAsPerBooks'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/operatingMarginAsPerBooks'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/operatingMarginAsPerBooks'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(62,1))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Estimated_OM'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/netProfitAsPerBooks'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/netProfitAsPerBooks'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/netProfitAsPerBooks'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(63,1))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Estimated_NetProfit'))
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Next'))

WebUI.delay(2)

//Banking Details

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/beneficiaryName'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/beneficiaryName'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/beneficiaryName'), 'Basava')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Radio_Button'))

WebUI.delay(2)
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(64,1) == "yes")	{
	
	WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input_Remarks (as per Bank)_ku-checkbox-secondaryBankAccountCheck'))
	WebUI.delay(1)
	
	WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__ku-text-numberSecondaryAccounts'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(65,1))
	WebUI.delay(3)
	
	for(def numberOfSecondaryAcc = 1; numberOfSecondaryAcc <= (findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getRowNumbers()); numberOfSecondaryAcc++)	{
		//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__radio-group-secondaryOrOtherAccount'))
		String xpath43 = "(//input[@name='radio-group-secondaryOrOtherAccount'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(66,1))+ "']"
		to.addProperty("xpath", ConditionType.EQUALS, xpath43)
		WebUI.click(to)
		WebUI.delay(2)
		
		WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__ku-text-secondaryAccountIfscCode'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(67,1))
		WebUI.delay(4)
		
		WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__ku-text-secondaryBankAccountNumber'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(68,1))
		WebUI.delay(1)
		
		WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__ku-text-secondaryAccountBeneficiaryName'), "Reena")
		WebUI.delay(1)
		
		//WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__radio-group-secondaryAccountType'))
		String xpath44 = "(//input[@name='radio-group-secondaryAccountType'])[@value='" +(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(69,1))+ "']"
		to.addProperty("xpath", ConditionType.EQUALS, xpath44)
		WebUI.click(to)
		WebUI.delay(2)
		
		WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/input__ku-text-vintageSecondaryBankAccount'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(70,1))
		WebUI.delay(1)
		
		WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/SecondaryBankdetails/span_Next'))
		WebUI.delay(2)
	}
	
}
else	{
	WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))
	
	WebUI.delay(2)
}

//Other Details
//Property Details
WebUI.sendKeys(findTestObject('Object Repository/PD1/PropertyDetails/Page_Svakarma Finance/input_Name of the OwnerOwners of the Property_ku-text-ownersOfProperty'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(71,1))
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/PropertyDetails/Page_Svakarma Finance/input_Land Area (In Case of Independent House)(sqft)_ku-text-propertyLandArea'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(72,1))
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/PropertyDetails/Page_Svakarma Finance/input_Built-up Area_ku-text-propertyBuilt-upArea'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(73,1))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/PropertyDetails/Page_Svakarma Finance/div_OccupiedStatus'))
WebUI.delay(2)
//WebUI.click(findTestObject('Object Repository/PD1/PropertyDetails/Page_Svakarma Finance/div_OccupiedStatus'))
String xpath45 = "//span[@name='" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(74,1) + "']"
//TestObject to = new TestObject("path")
println(xpath45)
to.addProperty("xpath", ConditionType.EQUALS, xpath45)
WebUI.click(to)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))
WebUI.delay(2)

//Existing Loans
WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))
WebUI.delay(2)

//Documents
//WebUI.scrollToElement(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(75,1) == "yes")	{
	//Pan verify
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Personal Documents_ku-checkbox-panVerify'))
	WebUI.delay(2)
}
//Voter Id
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(76,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_concat(Applicant  s Aadhar)_ku-checkbox-voterIDApplicantVerify'))
	WebUI.delay(2)
}
//Driving License Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(77,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_concat(Applicant  s Voter ID)_ku-checkbox-applicantsDrivingLicenseVerify'))
	WebUI.delay(2)
}
//Passport Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(78,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_concat(Applicant  s Driving License)_ku-checkbox-applicantPassportVerify'))
	WebUI.delay(2)
}
//BusinessPan Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(79,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Enterprise Documents_ku-checkbox-businessPanVerify'))
	WebUI.delay(2)
}
//BillBook Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(80,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Seen  verified Business PAN_ku-checkbox-billBookVerify'))
	WebUI.delay(2)
}
//BusinessProof Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(81,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Seen  Verified ITRs (Last to Last Year)_ku-checkbox-businessProofVerify'))
	WebUI.delay(2)
}
//GumasthaBook Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(82,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Seen  Verified Business Proof_ku-checkbox-gumasthaBookVerify'))
	WebUI.delay(2)
}
//GSTReturns Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(83,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Seen  Verified Gumastha Book_ku-checkbox-gstReturnsVerify'))
	WebUI.delay(2)
}
//BankStatement Verified
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(84,1) == "yes")	{
	WebUI.click(findTestObject('Object Repository/PD1/Documents/Page_Svakarma Finance/input_Seen  Verified Receipts_ku-checkbox-bankStatementsVerify'))
	WebUI.delay(2)
}

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))
WebUI.delay(2)

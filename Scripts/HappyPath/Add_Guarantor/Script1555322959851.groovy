import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/input__ku-checkbox-guarantorCheck'))
WebUI.delay(3)

WebUI.sendKeys(findTestObject('Object Repository/RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input_concat(Guarantor  s PAN)_ku-text-panGuarantor'), 'AAAPL2892C')

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__ku-text-guarantorFirstName'), 'Aul')

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__ku-text-guarantorLastName'), 'I')

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input_concat(Father  s Name)_ku-text-guarantorFathersName'), 'Anup')
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__radio-group-Gender'))
WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input_Single_radio-group-undefined'))
WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__ku-datepicker-guarantorDob'))
WebUI.delay(2)
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/div_2019'))
WebUI.delay(1)
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/span_2003'))
WebUI.delay(2)
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/span_13'))
WebUI.delay(2)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__ku-text-guarantorAddressLine1'), '#34 zxcv')
WebUI.delay(1)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input_Please enter a Pincode from Maharashtra_ku-text-guarantorPincode'), '444001')
WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__ku-text-guarantorMobile'))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/input__ku-text-guarantorMobile'), '9898984563')
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/div_Education'))
WebUI.delay(1)
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/div_Undergraduate'))

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/ID_Proof_CheckBox'))

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/Bureau_CheckBox'))
WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/span_Next'))
WebUI.delay(2)
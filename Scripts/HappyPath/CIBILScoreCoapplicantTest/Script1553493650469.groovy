import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebDriver driver =  DriverFactory.getWebDriver()

//Open Browser
WebUI.openBrowser("http://dev-bo.svakarma.net/auth/login")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/input_Enter User Name..._ku-te'), "credit.rm.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/input_Enter Password..._ku-tex'), "credit.rm.1")

//Click on Submit to Login
WebUI.click(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/span_Sign in'))
WebUI.delay(1)

//Click to Select the Lead
//WebUI.click(findTestObject('BackOfficeAfterPD1/Page_SelectLead/td_SelectLead'))

String xpath = '//td[contains(text(),"' + findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(1,1) + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(2)

//Select Co-appicant in tab
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/li_Co-applicant Details'))
WebUI.delay(1)

//click on Edit
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/span_EDIT'))
WebUI.delay(2)

for(def rowNum=1; rowNum <=findTestData('Data Files/CoApplicant/CoapplicantDetail').getRowNumbers(); rowNum++)
{

//click on Add-co applicant
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/span_ Add co-applicant'))
WebUI.delay(2)

//Add PAN
WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/input_PAN Co-Applicant_ku-text-panCoApplicant'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(2, rowNum))
WebUI.delay(1)



//Add first name
WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/input_First Name_ku-text-firstNameCoApplicant'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(3, rowNum))

//Add last name
WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/input_Last Name_ku-text-lastNameCoApplicant'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(4, rowNum))

//Add father name
WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/input_concat(Father  s Name)_ku-text-coApplicantFathersName'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(5, rowNum))
WebUI.delay(1)

//Click on Gender
WebUI.click(findTestObject('CIBILScoreCoapplicant/Page_AddCoapplicant/div_Gender')) 
WebUI.delay(1)

//Select Gender
//WebUI.click(findTestObject('CIBILScoreCoapplicant/Page_AddCoapplicant/div_Female'))

String xpath1 = '//div[contains(text(),"' + findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(6,rowNum) + '")]'
println(xpath1)
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath1)
WebUI.click(to)
WebUI.delay(1)

//Click on Status
WebUI.click(findTestObject('CIBILScoreCoapplicant/Page_AddCoapplicant/div_Status'))
WebUI.delay(1)

//Select status
//WebUI.click(findTestObject('CIBILScoreCoapplicant/Page_AddCoapplicant/div_Married'))
String xpath2 = '//div[contains(text(),"' + findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(7,rowNum) + '")]'
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath2)
WebUI.click(to)
WebUI.delay(1)

//Click on calander
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_Calander/input_Date of Birth_ku-text-dobCoApplicant'))
WebUI.delay(2)

//Click on Year
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_Calander/div_2019'))
WebUI.delay(2)

//Select Year
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_Calander/span_1973'))
WebUI.delay(2)

//Select date
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_Calander/span_27'))
WebUI.delay(2)

//Select Ok
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_Calander/span_OK'))
WebUI.delay(2)

//Click on Relation with Applicant
WebUI.click(findTestObject('CIBILScoreCoapplicant/Page_AddCoapplicant/div_RelationShip'))
WebUI.delay(1)

//Select RelationShip
//WebUI.click(findTestObject('CIBILScoreCoapplicant/Page_AddCoapplicant/div_Parent'))
String xpath3 = '//div[contains(text(),"' + findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(8,rowNum) + '")]'
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath3)
WebUI.click(to)
WebUI.delay(1)

//Enter Address
WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/textarea_Address Line 1_ku-text-coApplicantAddressLine1'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(9, rowNum))

//Enter pincode
WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/input_Pincode_ku-text-coApplicantPincode'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(10, rowNum))
WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_BackOffice  Login/input_Mobile Number_ku-text-coApplicantMobile'))
WebUI.delay(3)

WebUI.sendKeys(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_BackOffice  Login/input_Mobile Number_ku-text-coApplicantMobile'), findTestData('Data Files/CoApplicant/CoapplicantDetail').getValue(11, rowNum))
WebUI.delay(3)

}

//Save 
WebUI.click(findTestObject('Object Repository/CIBILScoreCoapplicant/Page_AddCoapplicant/Page_BackOffice  Login/span_SAVE'))















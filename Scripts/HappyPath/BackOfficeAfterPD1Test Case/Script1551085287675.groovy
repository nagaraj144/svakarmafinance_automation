import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser(findTestData('Data Files/Credentials/Cred_Data').getValue(2, 4))

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/input_Enter User Name..._ku-te'), findTestData('Data Files/Credentials/Cred_Data').getValue(3, 4))

//Enter Password
WebUI.sendKeys(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/input_Enter Password..._ku-tex'), findTestData('Data Files/Credentials/Cred_Data').getValue(4, 4))

//Click on Submit to Login
WebUI.click(findTestObject('BackOfficeAfterPD1/Credit.rm.1Login/span_Sign in'))
WebUI.delay(1)

//Click to Select the Lead
//WebUI.click(findTestObject('BackOfficeAfterPD1/Page_SelectLead/td_SelectLead'))

String xpath = ('//td[contains(text(), "' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(1, 1)) + '" )]'

println(xpath)

TestObject to = new TestObject('path')

to.addProperty('xpath', ConditionType.EQUALS, xpath)

WebUI.click(to)

WebUI.delay(1)

//Select Bureau Details
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/li_Bureau Details'))
WebUI.delay(1)

//Edit to Enter CibilScore
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/span_EDIT'))

//Enter Applicant CibilScore
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_CIBIL Score - Applicant_'),
	Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_CIBIL Score - Applicant_'), findTestData('Data Files/BackOfficeAfterPD1/BureauDetails').getValue(1, 1))
WebUI.delay(2)

//Enter Co-Applicant CibilScore
for(def numberOfCoapplicants = 0; numberOfCoapplicants < (findTestData('Data Files/BackOfficeAfterPD1/BureauDetails').getRowNumbers()); numberOfCoapplicants++) {
	println(numberOfCoapplicants)
	String xpath1 = ("//input[@id='cibilScoreApplicantCo_"+ numberOfCoapplicants +"']")
	println(xpath1)
	to.addProperty('xpath', ConditionType.EQUALS, xpath1)
	WebUI.sendKeys(to,
		Keys.chord(Keys.CONTROL, 'a'))
	WebUI.sendKeys(to, findTestData('Data Files/BackOfficeAfterPD1/BureauDetails').getValue(2, numberOfCoapplicants+1))

}
WebUI.delay(2)
//Enter Applicant Highmarks
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_Highmark Score - Applicant_ku-text-highmarkScoreApplicant'),
	Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/input_Highmark Score - Applicant_ku-text-highmarkScoreApplicant'), findTestData('Data Files/BackOfficeAfterPD1/BureauDetails').getValue(3, 1))
WebUI.delay(2)

//Enter Co-Applicant Highmarks details
for(def numberOfCoapplicant = 0; numberOfCoapplicant < (findTestData('Data Files/BackOfficeAfterPD1/BureauDetails').getRowNumbers()); numberOfCoapplicant++) {
	println(numberOfCoapplicant)
	String xpath2 = ("//input[@id='highmarkScoreApplicantCo_"+ numberOfCoapplicant +"']")
	println(xpath2)
	to.addProperty('xpath', ConditionType.EQUALS, xpath2)
	WebUI.sendKeys(to,
		Keys.chord(Keys.CONTROL, 'a'))
	WebUI.sendKeys(to, findTestData('Data Files/BackOfficeAfterPD1/BureauDetails').getValue(4, numberOfCoapplicant+1))
	
}

WebUI.delay(2)

//Save all the Details Edited
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/span_SAVE'))
WebUI.delay(8)

WebUI.refresh()
WebUI.delay(3)

//Approve the Details
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_EnterBureauDetails/span_Approve'))
WebUI.delay(3)

//Enter the comments
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD1/Page_CommentAndSubmit/textarea_Enter Your Comment...'), "Procced")

//Submit
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD1/Page_CommentAndSubmit/span_Submit'))
WebUI.delay(3)

//Close browser
WebUI.closeBrowser()









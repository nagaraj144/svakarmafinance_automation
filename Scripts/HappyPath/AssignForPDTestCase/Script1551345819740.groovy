import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys


	//Open Browser
	WebUI.openBrowser(findTestData('Data Files/Credentials/Cred_Data').getValue(2, 2))
	
	//Maximizing the window
	WebUI.maximizeWindow()
	
	//Enter Mail
	WebUI.sendKeys(findTestObject('AssignForPD/SalesManagerLogin/Page_Lead Management/input__jss9'), findTestData('Data Files/Credentials/Cred_Data').getValue(3, 2))
	
	//Enter Password
	WebUI.sendKeys(findTestObject('AssignForPD/SalesManagerLogin/Page_Lead Management/input__password'), findTestData('Data Files/Credentials/Cred_Data').getValue(4, 2))
	
	//Click on Login
	WebUI.click(findTestObject('AssignForPD/SalesManagerLogin/Page_Lead Management/input__jss9 jss12'))
	WebUI.delay(2)

for(def rowNum=1; rowNum <= findTestData('Data Files/LeadGenerationAndAssign/Assigning').getRowNumbers(); rowNum++)
{
	
String xpath = '//div[contains(text(),"' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(1,rowNum) + '")]'
println(xpath)
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)

WebUI.delay(2)

//Click on AssignPD
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/Page_Lead Management/span_Assign for PD'))
WebUI.delay(2)

//Assign for pre-qualification
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/div_'))
WebUI.delay(2)

//Select in DropDown
//WebUI.click(findTestObject('AssignForPD/Page_Lead Management/li_relationship.manager.1dev.c'))
String xpath1 = '//li[contains(text(),"' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(2,rowNum) + '")]'
println(xpath1)
//TestObject to = new TestObject("path")
println(xpath1)
to.addProperty("xpath", ConditionType.EQUALS, xpath1)
WebUI.click(to)


WebUI.delay(2)

//Assign for PD2
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/div_2'))
WebUI.delay(2)

//Select in DropDown
//WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/li_credit.rm.1dev.com'))
String xpath2 = '//li[contains(text(),"' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(3,rowNum) + '")]'
println(xpath2)
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath2)
WebUI.click(to)

WebUI.delay(2)

//Click on confirm
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/span_Confirm'))
WebUI.delay(2)

}
//WebUI.callTestCase(findTestCase("Test Cases/HappyPath/RelationshipManagerForPre-QualTestCase"), null)

WebUI.closeBrowser()


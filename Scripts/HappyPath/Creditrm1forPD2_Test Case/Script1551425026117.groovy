import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By as By

//Open Browser
WebUI.openBrowser(findTestData('Data Files/Credentials/Cred_Data').getValue(2, 5))

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/input_Enter Email Id_ku-text-e'), findTestData('Data Files/Credentials/Cred_Data').getValue(3, 5))

//Enter Password
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/input_Enter Password_ku-text-p'), findTestData('Data Files/Credentials/Cred_Data').getValue(4, 5))

//Select CheckBox
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/input_This field is required_k'))

//Click on Submit to Login
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/span_Submit'))
WebUI.delay(2)

//Click to Select the Lead
String xpath = ('//span[contains(text(), "' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(1, 1)) + '" )]'
println(xpath)
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(2)

//Person Accompanied
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PDDetails/input_Person who accompanied t'), findTestData('Data Files/PD2/CrmToPD2').getValue(1, 1))

//Name of Person met
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PDDetails/input__ku-text-nameOfPersonMet'), findTestData('Data Files/PD2/CrmToPD2').getValue(2, 1))
WebUI.delay(2)

//Designation
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_Designation'))
WebUI.delay(2)

//Select as
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_Applicant'))
String xpath1 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(3,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath1)
WebUI.click(to)
WebUI.delay(2)

//Queries
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_queries'))
WebUI.delay(2)

//Select as
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_Borrower Himself'))
String xpath2 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(4,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath2)
WebUI.click(to)
WebUI.delay(2)

//Click on Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/span_Next'))
WebUI.delay(2)

//LoanAmount Required
//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/span_Next'))
WebUI.delay(2)

//Name Missmatch of Applicant
if(findTestData('Data Files/PD2/CrmToPD2').getValue(5,1) == "yes") {
	String xpath3 = "(//input[@name='radio-group-nameMismatchPan'])[@value='yes']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath3)
	WebUI.click(to)
	WebUI.delay(2)
	
	//Comment on Name Matching
	//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/input__ku-text-commentOnNameMi'), "Procced")
	String xpath4 = "//input[@id='ku-text-commentOnNameMismatch']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath4)
	WebUI.sendKeys(to,
		Keys.chord(Keys.CONTROL, 'a'))
	WebUI.sendKeys(to, "Process")
	WebUI.delay(2)
}
else {
	String xpath5 = "(//input[@name='radio-group-nameMismatchPan'])[@value='no']"
	to.addProperty("xpath", ConditionType.EQUALS, xpath5)
	WebUI.click(to)
	WebUI.delay(2)
}
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/span_Next'))
WebUI.delay(3)

//Name Missmatch of Co-Applicant
for(noOfCoapplicant=1; noOfCoapplicant <= (findTestData('Data Files/PD2/CrmToPD2').getRowNumbers()); noOfCoapplicant++) {
	println(findTestData('Data Files/PD2/CrmToPD2').getValue(5,noOfCoapplicant) == "yes")
if(findTestData('Data Files/PD2/CrmToPD2').getValue(5,noOfCoapplicant) == "yes") {
	String xpath6 = "(//input[@name='radio-group-nameMismatchPanCoApplicant'])[@value='yes']"
	to.addProperty("xpath", ConditionType.EQUALS, xpath6)
	WebUI.click(to)
	WebUI.delay(2)
	
	//Comment on Name Matching
	//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/input__ku-text-commentOnNameMi'), "Procced")
	String xpath7 = "//input[@id='ku-text-commentOnNameMismatchCoApplicant']"
	to.addProperty("xpath", ConditionType.EQUALS, xpath7)
	WebUI.sendKeys(to,
		Keys.chord(Keys.CONTROL, 'a'))
	WebUI.sendKeys(to, "Process")
	WebUI.delay(2)
}
else {
	String xpath8 = "(//input[@name='radio-group-nameMismatchPanCoApplicant'])[@value='no']"
	to.addProperty("xpath", ConditionType.EQUALS, xpath8)
	WebUI.click(to)
	WebUI.delay(2)
}
WebUI.delay(2)

//Next for coapplicant
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/span_Next'))
WebUI.delay(3)
}

//Next for Address tab
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails2/span_Next'))
WebUI.delay(3)

//Household Details
//No. of members in home
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-membersInHouseh'),
	Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-membersInHouseh'), findTestData('Data Files/PD2/CrmToPD2').getValue(6,1))

//No. of Adults
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-adultMembers'),
	Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-adultMembers'), findTestData('Data Files/PD2/CrmToPD2').getValue(7,1))

//No. of Children
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-childrenMembers'),
	Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-childrenMembers'), findTestData('Data Files/PD2/CrmToPD2').getValue(8,1))

//Select as
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_Parent'))
for(def familymembers=1; familymembers <= findTestData('Data Files/PD2/CrmToPD2').getRowNumbers(); familymembers++) {

//Relation
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_Relation'))
String xpath9 = "//div[@id='relationOutsideMember_"+ (familymembers - 1) +"']/div/div[2]"
println(xpath9)
to.addProperty("xpath", ConditionType.EQUALS, xpath9)
WebUI.click(to)
WebUI.delay(2)

//Select as
String xpath10 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(9,familymembers) + "')]"
println(xpath10)
to.addProperty("xpath", ConditionType.EQUALS, xpath10)
WebUI.click(to)
WebUI.delay(2)

//Occupation
//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/textarea_Frequency_ku-text-occ'), findTestData('Data Files/PD2/CrmToPD2').getValue(10,familymembers))
String xpath11 = "//textarea[@id='occupationoutsideMember_"+ (familymembers - 1) +"']"
println(xpath11)
to.addProperty("xpath", ConditionType.EQUALS, xpath11)
WebUI.sendKeys(to, findTestData('Data Files/PD2/CrmToPD2').getValue(10,familymembers))
WebUI.delay(2)

//Age
//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input_Frequency_ku-text-ageOut'), findTestData('Data Files/PD2/CrmToPD2').getValue(11,familymembers))
String xpath12 = "//input[@id='ageOutsideMember_"+ (familymembers - 1) +"']"
println(xpath12)
to.addProperty("xpath", ConditionType.EQUALS, xpath12)
WebUI.sendKeys(to, findTestData('Data Files/PD2/CrmToPD2').getValue(11,familymembers))
WebUI.delay(2)

//Address
//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/textarea_Frequency_ku-text-add'), "#24 jdfbvkjvb")
String xpath13 = "//textarea[@id='addressOfOutsideMember_"+ (familymembers - 1) +"']"
println(xpath13)
to.addProperty("xpath", ConditionType.EQUALS, xpath13)
WebUI.sendKeys(to, "#34, south avenue, Tarwala Nagar, Panchavati Nashik")
WebUI.delay(2)

//Marital Status
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_MaritalStatus'))
String xpath14 = "//div[@id='maritalStatusOutsideMember_"+ (familymembers - 1) +"']/div/div[2]"
println(xpath14)
to.addProperty("xpath", ConditionType.EQUALS, xpath14)
WebUI.click(to)
WebUI.delay(2)

//Select as
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_Unmarried'))
String xpath15 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(12,familymembers) + "')]"
to.addProperty("xpath", ConditionType.EQUALS, xpath15)
WebUI.click(to)
WebUI.delay(1)

//Income
//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input_Frequency_ku-text-income'), "100000")
String xpath16 = "//input[@id='incomeOutsideMember_"+ (familymembers - 1) +"']"
println(xpath16)
to.addProperty("xpath", ConditionType.EQUALS, xpath16)
WebUI.sendKeys(to, "10000")
WebUI.delay(2)

//Expense
//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input_Frequency_ku-text-expens'), "10000")
String xpath17 = "//input[@id='expenseOutsideMember_"+ (familymembers - 1) +"']"
println(xpath17)
to.addProperty("xpath", ConditionType.EQUALS, xpath17)
WebUI.sendKeys(to, "10000")
WebUI.delay(2)

//Add row
WebUI.click(findTestObject('Object Repository/Credit_rm1for_PD2/Page_HouseholdDetails/span_ Add Row'))
WebUI.delay(2)
}
//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/span_Next'))
WebUI.delay(2)
for(def Expense=1; Expense <= findTestData('Data Files/PD2/CrmToPD2').getRowNumbers(); Expense++)	{

//Nature of Expense
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/div_NatureofExpense'))
	String xpath18 = "//div[@id='expenseHousehold_"+ (Expense - 1) +"']/div/div[2]"
	println(xpath18)
	to.addProperty("xpath", ConditionType.EQUALS, xpath18)
	WebUI.click(to)
	WebUI.delay(2)

//Select as
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/div_Living Expenses'))
	String xpath19 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(13,Expense) + "')]"
	to.addProperty("xpath", ConditionType.EQUALS, xpath19)
	WebUI.click(to)
	WebUI.delay(2)

//Monthly Amount
//WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/input_Other Information_ku-tex'), "10000")
	String xpath20 = "//input[@id='amountExpenseHousehold_"+ (Expense - 1) +"']"
	to.addProperty("xpath", ConditionType.EQUALS, xpath20)
	WebUI.sendKeys(to, findTestData('Data Files/PD2/CrmToPD2').getValue(14,Expense))
	WebUI.delay(2)
	
//Other information
//SWebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/textarea_Other Information_ku-'), "svhjdv")

	if(Expense < 5)	{
		//Add Row
		WebUI.click(findTestObject('Object Repository/Credit_rm1for_PD2/Page_HouseholdDetails2/span_ Add Row'))
	}

}

//Comments on Household
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/textarea_Comments on Household'), "Household details are almost seen ")

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/span_Next2'))
WebUI.delay(2)

//Years in Residense
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/input__ku-text-noOfYearsInResi'), findTestData('Data Files/PD2/CrmToPD2').getValue(15,1))

//Ownership
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_OwnershipStatus'))
WebUI.delay(2)

//Select as
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_Family Owned'))
String xpath21 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(16,1) + "')]"
to.addProperty("xpath", ConditionType.EQUALS, xpath21)
WebUI.click(to)
WebUI.delay(2)

//Type of Structure
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_TypeofStructure'))
WebUI.delay(2)

//Select as 
//WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_Flats and Bunglow'))
String xpath22 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(17,1) + "')]"
to.addProperty("xpath", ConditionType.EQUALS, xpath22)
WebUI.click(to)
WebUI.delay(2)

//Comment
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/textarea_Comments on Residence'), "Good Residence location")
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/span_Next'))
WebUI.delay(3)

//Asset details
for(def Asset=1; Asset <= findTestData('Data Files/PD2/CrmToPD2').getRowNumbers(); Asset++)	{
//WebUI.click(findTestObject('Object Repository/Credit_rm1for_PD2/Page_OtherIncome/div_AssetDescription'))
String xpath23 = "//div[@id='assetType_"+ (Asset - 1) +"']/div/div[2]"
to.addProperty("xpath", ConditionType.EQUALS, xpath23)
WebUI.click(to)
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/Credit_rm1for_PD2/Page_OtherIncome/div_Business Setup'))
String xpath24 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/PD2/CrmToPD2').getValue(18,Asset) + "')]"
println(xpath24)
to.addProperty("xpath", ConditionType.EQUALS, xpath24)
WebUI.click(to)
WebUI.delay(2)

//WebUI.sendKeys('Object Repository/Credit_rm1for_PD2/Page_OtherIncome/input_Source of Funds_acquisitionValue_0', findTestData('Data Files/PD2/CrmToPD2').getValue(18,Asset))
String xpath25 = "//input[@id='acquisitionValue_"+ (Asset - 1) +"']"
to.addProperty("xpath", ConditionType.EQUALS, xpath25)
WebUI.sendKeys(to, findTestData('Data Files/PD2/CrmToPD2').getValue(19,Asset))
WebUI.delay(2)

	if(Asset < 7) {
		//Add
		WebUI.click(findTestObject('Object Repository/Credit_rm1for_PD2/Page_OtherIncome/span_ Add Row'))
	}

}

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome/span_Next'))
WebUI.delay(2)

//Life style 
WebUI.click(findTestObject(''))
WebUI.delay(1)

WebUI.click(findTestObject(''))
WebUI.delay(1)

WebUI.click(findTestObject(''))
WebUI.delay(1)

WebUI.click(findTestObject(''))
WebUI.delay(1)

WebUI.click(findTestObject(''))
WebUI.delay(1)

WebUI.click(findTestObject(''))
WebUI.delay(2)

WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/div_WhiteGoods'))
WebUI.delay(2)

WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/div_Several'))
WebUI.delay(1)

//LIC
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/input_LIC_ku-text-lic'), "10000000")

//Jewellery
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/input_Jewellery (Approx. value'), "1000000")
WebUI.delay(1)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/span_Next'))
WebUI.delay(2)

//Comments on Documents
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_Documents/textarea_Final Comments on the'), "sdbvdshfbhdbv")

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_Documents/span_Next'))
WebUI.delay(2)

//Product1 units
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 1_ku-text-unitRe'), "100")
WebUI.delay(1)

//Product1 Rupees
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 1_ku-text-rupees'), "1000000")
WebUI.delay(1)

//Product2 units
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 2_ku-text-unitRe'), "120")
WebUI.delay(1)

//Product2 Rupees
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 2_ku-text-rupees'), "2000000")
WebUI.delay(1)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow/span_Next'))
WebUI.delay(2)

//Next2
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow2/span_Next'))
WebUI.delay(2)

//Next3
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow3/span_Next'))
WebUI.delay(2)

//Next4
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow4/span_Next'))
WebUI.delay(2)

//Next5
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow5/span_Next'))
WebUI.delay(2)

//Next6
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow6/span_Next'))
WebUI.delay(2)

//Next7
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow7/span_Next'))
WebUI.delay(2)

//Next8
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow8/span_Next'))
WebUI.delay(2)

//Next9
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow9/span_Next'))
WebUI.delay(2)

//Next10
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow10/span_Next'))
WebUI.delay(2)

//SuccessScreen
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_SuccessScreen/span_Next'))
WebUI.delay(2)

//FinalApproval
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_FinalApproval/div_FinalApproval'))
WebUI.delay(2)

//Approve
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_FinalApproval/div_Approve'))
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_FinalApproval/span_Next'))
WebUI.delay(3)

//Close Browser
WebUI.closeBrowser()






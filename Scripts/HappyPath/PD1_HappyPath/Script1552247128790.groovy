import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.lang.reflect.Array

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

WebUI.openBrowser(null)

WebUI.maximizeWindow(null)

'Login'
WebUI.navigateToUrl(findTestData('Data Files/Credentials/Cred_Data').getValue(2, 3))

//WebUI.sendKeys(findTestObject('Login/input_Enter Email Id_ku-text-e'), UserName)
WebUI.setText(findTestObject('Login/input_Enter Email Id_ku-text-e'), findTestData('Data Files/Credentials/Cred_Data').getValue(3, 3))

//WebUI.sendKeys(findTestObject('Login/input_Enter Password_ku-text-p'), Password)
WebUI.sendKeys(findTestObject('Login/input_Enter Password_ku-text-p'), findTestData('Data Files/Credentials/Cred_Data').getValue(4, 3))

WebUI.click(findTestObject('Login/input_Enter Password_ku-checkb'))

WebUI.click(findTestObject('Login/span_Submit'))

WebUI.delay(1)

//Select Lead for PD1
//WebUI.click(findTestObject('PD1/NavigateToPD1/LoanID_1927000073'))
String xpath = ('//span[contains(text(), "' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(1, 1)) + '" )]'

println(xpath)

TestObject to = new TestObject('path')

to.addProperty('xpath', ConditionType.EQUALS, xpath)

WebUI.click(to)

Thread.sleep(2)

WebUI.delay(1)

/*//Filling PD Details Information
WebUI.click(findTestObject('Object Repository/PD1/PD Details/div_PD Details'))

//WebUI.click(findTestObject('PD1/NavigateToPD1/PD1_Next'))
WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/Page_Svakarma Finance/input_Person who accompanied the PD officer_ku-text-coOfficerR'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/Page_Svakarma Finance/input_Person who accompanied the PD officer_ku-text-coOfficerR'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/Page_Svakarma Finance/input_Person who accompanied the PD officer_ku-text-coOfficerR'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(15, 1))

WebUI.click(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(16, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/DesignationOfPersonMet_Dropdown'))
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/PD Details/DropdownValue_Applicant'))
String xpath8 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(17,1) + '")]'
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath8)
WebUI.click(to)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/Who were the queries answ_Dropdown'))

WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/PD1/PD Details/Page_Svakarma Finance/div_Relative'))
String xpath9 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(18,1) + '")]'
 //TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath9)
WebUI.click(to)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/PD Details_Next'))

WebUI.delay(2)

//Filling the Loan Requirements Tab
WebUI.scrollToElement(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'), 1)

println(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(19,1) == "yes")
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(19,1) == "yes") {
	String xpath23 = "(//input[@name='radio-group-newToCreditCustomer'])[@value='yes']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath23)
	WebUI.click(to)
	WebUI.delay(2)
}
else {
	String xpath24 = "(//input[@name='radio-group-newToCreditCustomer'])[@value='no']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath24)
	WebUI.click(to)
	WebUI.delay(2)
}
WebUI.click(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'), Keys.chord(Keys.BACK_SPACE))

WebUI.click(findTestObject('PD1/LoanRequirements/ScehemeValue_WORKING CAPITAL MSME'))

WebUI.scrollToElement(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), 0)

WebUI.click(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), '4500000')

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-commentsOnSecurity'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-commentsOnSecurity'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-commentsOnSecurity'), 'Good security')

WebUI.click(findTestObject('PD1/LoanRequirements/LoanRequirement_Next'))

//Filling the Personal Details Tab
///WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/input_Yes_radio-group-undefine - Copy'))
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/input__radio-group-undefined'))

WebUI.scrollToElement(findTestObject('Object Repository/PD1/Personal_Details/ID_Proof_CheckBox'), 0)
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Personal_Details/input__ku-text-fathersName'), 'Agarwal')

//Add co-applicant
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/input_Voter ID_ku-checkbox-coApplicantCheck'))
WebUI.delay(2)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/input__ku-text-numberCoApplicant'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(20, 1))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/ID_Proof_CheckBox'))

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/Bureau_CheckBox'))
WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/span_Next'))
WebUI.delay(3)

//Add co-applicant details

for(def numberOfCoapplicants = 1; numberOfCoapplicants <= (findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getRowNumbers()); numberOfCoapplicants++) {
println(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getRowNumbers())
println(numberOfCoapplicants)

WebUI.sendKeys(findTestObject('Object Repository/RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input_PAN_ku-text-panCoApplicant'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(21, 1))
WebUI.delay(3)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-firstNameCoApplicant'),  Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-firstNameCoApplicant'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-firstNameCoApplicant'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(22, numberOfCoapplicants))
WebUI.delay(1)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-lastNameCoApplicant'),  Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-lastNameCoApplicant'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-lastNameCoApplicant'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(23, numberOfCoapplicants))
WebUI.delay(1)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantFathersName'),  Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantFathersName'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantFathersName'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(24, numberOfCoapplicants))
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__radio-group-gender'))
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input_Single_radio-group-undefined'))
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-datepicker-dobCoApplicant'))
WebUI.delay(2)
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_Guarantor-Detils/div_2019'))
WebUI.delay(2)
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/span_1995'))
String xpath10 = '//span[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(25, numberOfCoapplicants) + '")]'
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath10)
WebUI.click(to)

WebUI.delay(2)
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/span_8'))
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/div_Relationship'))
WebUI.delay(2)
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/div_Parent'))
String xpath11 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(26, numberOfCoapplicants) + '")]'
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath11)
WebUI.click(to)

WebUI.delay(2)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantAddressLine1'),  Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantAddressLine1'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantAddressLine1'), '#43, commertial street, Tarwala Nagar, Panchavati Nashik')
WebUI.delay(1)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input_Please enter a Pincode from Maharashtra_ku-text-coApplicantPincode'),  Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input_Please enter a Pincode from Maharashtra_ku-text-coApplicantPincode'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input_Please enter a Pincode from Maharashtra_ku-text-coApplicantPincode'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(27, numberOfCoapplicants))
WebUI.delay(3)

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantMobile'),  Keys.chord(Keys.CONTROL, 'a'))
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantMobile'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/input__ku-text-coApplicantMobile'), '9898989898')
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/div_EducationLevel'))
WebUI.delay(2)

//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/div_Undergraduate'))
String xpath12 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(28, numberOfCoapplicants) + '")]'
to.addProperty("xpath", ConditionType.EQUALS, xpath12)
WebUI.click(to)
WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_Add-coapplicant/Page_coapplicant-Details/span_Next'))
WebUI.delay(3)
}*/

//Filling the Business Details Tab
//Address and Contact Details
WebUI.click(findTestObject('Object Repository/PD1/Business_Details/AddressAndContactDetails/span_Next'))
WebUI.delay(3)

//Business Details
WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/Datepicker-enterprises'))
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/div_2019'))
WebUI.delay(1)
//WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/span_2003'))
String xpath25 = ('//span[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(29, 1)) + '")]'
//TestObject to = new TestObject("path")
println(xpath25)
to.addProperty('xpath', ConditionType.EQUALS, xpath25)
WebUI.click(to)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/span_13'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/DocumenToVerifyDate_Dropdown'))

WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/DocumenToVerifyDate_Dropdown_Value'))
String xpath26 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(30, 1) + '")]'
println(xpath26)
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath26)
WebUI.click(to)
WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/PD1/Business_Details/Business_Details/GrapicalAreaOfCoverage_Dropdown'),
	0)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/GrapicalAreaOfCoverage_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/GrapicalAreaOfCoverage_Dropdown_Value'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/span_Next'))

WebUI.delay(2)

//Business Details - Customer
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Business Details_Customer/Text-productsService'),
	Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Business Details_Customer/Text-productsService'),
	Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Business Details_Customer/Text-productsService'),
	'Customer Goods')

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/modeOfAcquiringNewCustomers_Dropdown'))

WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/modeOfAcquiringNewCustomers_Dropdown_Value'))
String arr= findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(31,1)
String[] myData = arr.split(',')
for(String s : myData) {
	String xpath14 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + s + "')]"
	println xpath14
	to.addProperty("xpath", ConditionType.EQUALS, xpath14)
	WebUI.click(to)
}

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/clickOnBody'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(32, 1))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/TypeOfCustomers_Dropdown'))

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/Page_Svakarma Finance/div_B2B'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/clickOnBody'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'),
	Keys.chord(Keys.ESCAPE))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'),
	Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'),
	Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'),
	'Neeraj Enterprise Nilkaman Mart')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerAddress'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerAddress'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerAddress'), 'Ashoka High School, Ashoka Marg, Nashik HPT College, College Road, Nashik Railway Station, Nashik Road, Nashik')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerContact'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerContact'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerContact'), '9967845637')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_customerYearsRelationship'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_customerYearsRelationship'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_customerYearsRelationship'), '5')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerSales'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerSales'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerSales'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(33, 1))

WebUI.click(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Dropdown_CustomerPaymentTerms'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Dropdown_CustomerPaymentTerms_Value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/span_Next'))

//Management & Operations
WebUI.delay(2)

// Management
WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Management_Tab'))

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/WhoIsRunningOffice_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/WhoIsRunningOffice_Dropdown_Value'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-yearsInSameBusi'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-yearsInSameBusi'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-yearsInSameBusi'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(34, 1))
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-peopleActivelyI'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-peopleActivelyI'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-peopleActivelyI'), '1')
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Management_Tab'))

WebUI.delay(1)

// Operations
WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Operations_Tab'))
WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/People_Employeed'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/People_Employeed'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/People_Employeed'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(35, 1))
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Employeees_seen'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Employeees_seen'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Employeees_seen'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(36, 1))

WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Seasonality_Radio_Button'))
println(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(37,1) == "yes")
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(37,1) == "yes") {
	String xpath27 = "(//input[@name='radio-group-seasonality'])[@value='yes']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath27)
	WebUI.click(to)
	WebUI.delay(2)
}
else {
	String xpath28 = "(//input[@name='radio-group-seasonality'])[@value='no']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath28)
	WebUI.click(to)
	WebUI.delay(2)
}
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/BillBook_Radio_Button'))
WebUI.delay(1)

//WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Tool_Radio_Button'))
println(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(38,1) == "yes")
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(38,1) == "yes") {
	String xpath29 = "(//input[@name='radio-group-digitalSystem'])[@value='yes']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath29)
	WebUI.click(to)
	WebUI.delay(2)
}
else {
	String xpath30 = "(//input[@name='radio-group-digitalSystem'])[@value='no']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath30)
	WebUI.click(to)
	WebUI.delay(2)
}
WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Payment_Channel_Radio_Button'))
println(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(39,1) == "yes")
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(39,1) == "yes") {
	String xpath31 = "(//input[@name='radio-group-digitalPaymentCheck'])[@value='yes']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath31)
	WebUI.click(to)
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/div_digital_payment'))
	WebUI.delay(2)
	
	//$WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Page_Svakarma Finance/div_Wallet'))
	//String xpath15 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qual/Pre_qual').getValue(32,1) + '")]'
	String array= findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(40,1)
	String[] Data = array.split(',')
	for(String s : Data) {
		String xpath15 = "//div[contains(text(),'" + s + "')]"
		println xpath15
		to.addProperty("xpath", ConditionType.EQUALS, xpath15)
		WebUI.click(to)
	}
	
	WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/clickOnBody'))
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/input__ku-text-percentDigitalPayments'))
	WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/input__ku-text-percentDigitalPayments'), Keys.chord(Keys.CONTROL,
		'a'))
	
	WebUI.delay(1)
	
	WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/input__ku-text-percentDigitalPayments'), Keys.chord(Keys.BACK_SPACE))
	
	WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/input__ku-text-percentDigitalPayments'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(41, 1))
	WebUI.delay(2)
}
else {
	String xpath32 = "(//input[@name='radio-group-digitalPaymentCheck'])[@value='no']"
	//TestObject to = new TestObject("path")
	to.addProperty("xpath", ConditionType.EQUALS, xpath32)
	WebUI.click(to)
	WebUI.delay(2)
}
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Name'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Name'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Name'), 'Neeraj Enterprises ( Amit Sir ) Ashoka School Canteen Ajinkya Canteen ( Mr Sachin Toche ) ( HPT College ) Herinko Canteen ( Indian Railway Canteen )')
WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/contanct_number_of_people'), Keys.chord(
		Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/contanct_number_of_people'), Keys.chord(
		Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/contanct_number_of_people'), '9876456789')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/comments'), Keys.chord(Keys.CONTROL,
		'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/comments'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/comments'), 'Applicant doing his business with above customer regularly ( Customer is doing business with Niraj Enterprises since from 2 years, with Ajinkya Canteen since from 4 years, with Herinko since 3.5 years')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Refe_Check_Dropdown'))

//WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Dropdown_Value'))
String xpath16 = '//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(42,1) + '")]'
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath16)
WebUI.click(to)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Operations_Tab'))

WebUI.delay(2)

//Offices
WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_Tab'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfOffice_Dropdown'))

//WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfOffice_Dropdown_value'))
String xpath17 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(43,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath17)
WebUI.click(to)

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_OwnerShip_Dropdown'))

//WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_OwnerShip_Dropdown_Value'))
String xpath18 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(44,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath18)
WebUI.click(to)

WebUI.delay(2)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/yearsCurrentOffice'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/yearsCurrentOffice'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/yearsCurrentOffice'), '18')
WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfStructure_Dropdown'))
WebUI.delay(1)

//WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfStructure_Dropdown_Value'))
String xpath19 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(45,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath19)
WebUI.click(to)

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Name_Board_RadioButton'))
WebUI.delay(1)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/localityOfOffice_Dropdown'))

//WebUI.click(findTestObject('PD1/Management  Operations/Offices/localityOfOffice_Dropdown_Value'))
String xpath20 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(46,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath20)
WebUI.click(to)

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupShown_Dropdown'))
WebUI.delay(1)

//WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupShown_Dropdown_Value'))
String xpath21 = "//span[@role='menuitem']/div/div/div[contains(text(),'" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(47,1) + "')]"
//TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath21)
WebUI.click(to)
WebUI.delay(2)

WebUI.scrollToElement(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), 0)
WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupSuitability_Dropdown'))
WebUI.delay(2)

//WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupSuitability_Dropdown_Value'))
String xpath22 = "//span[@name='" + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(48,1) + "']"
//TestObject to = new TestObject("path")
println(xpath22)
to.addProperty("xpath", ConditionType.EQUALS, xpath22)
WebUI.click(to)
WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_RadioButton'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), '5/6')
WebUI.delay(1)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_Next'))

WebUI.delay(3)

//Calling test case
WebUI.callTestCase(findTestCase('HappyPath/Continuation_of_PD1'), null)
WebUI.delay(1)

//If the flow is selected as yes
if(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(85,1) == "yes")	{
String xpath46 = "(//input[@name='radio-group-cashFlowCheck'])[@value='yes']"
to.addProperty("xpath", ConditionType.EQUALS, xpath46)
WebUI.click(to)
WebUI.delay(2)

//Cash Flow Details
//Financial Statements
//WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Business_Cycle'))

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/FinancialStatement_Next'))
WebUI.delay(2)

//Financial Statement Computation
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Financial Statement Computation_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Financial Statement Computation_Next'))
WebUI.delay(2)

//Raw Material
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material_Next'))
WebUI.delay(2)

//Raw Material Computation
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material Computation_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material Computation_Next'))
WebUI.delay(2)

//Charges
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Charges_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Charges_Next'))
WebUI.delay(2)

//Charge Computation
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Charge Computation_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Charge Computation_Next'))
WebUI.delay(2)

//BalanceSheet
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet_Next'))
WebUI.delay(2)

//BalanceSheet2
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet2_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet2_Next'))
WebUI.delay(2)

//Totals
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Totals_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Totals_Next'))
WebUI.delay(2)

//Cash Flow Model
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Cash Flow Model_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Cash Flow Model_Next'))
WebUI.delay(2)

//Successs Screen
WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Successs Screen_Next'))
WebUI.delay(2)

//Decision
WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Decision_Dropdown'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/DecisionDropdown_Value'))
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Decision_Next'))
WebUI.delay(2)
}

//If the flow is selected as no
else {
		String xpath47 = "(//input[@name='radio-group-cashFlowCheck'])[@value='no']"
		to.addProperty("xpath", ConditionType.EQUALS, xpath47)
		WebUI.click(to)
		WebUI.delay(2)
		
		//Successs Screen
		WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Successs Screen_Next'))
		WebUI.delay(2)
		
		//Decision
		WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Decision_Dropdown'))
		WebUI.delay(2)
		
		WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/DecisionDropdown_Value'))
		WebUI.delay(2)
		
		WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Decision_Next'))
		WebUI.delay(2)
}

WebUI.closeBrowser()


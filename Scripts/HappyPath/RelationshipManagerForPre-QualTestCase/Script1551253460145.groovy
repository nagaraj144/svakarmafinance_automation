import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys

//Open Browser
//WebUI.openBrowser(var_url)
WebUI.openBrowser(findTestData('Data Files/Credentials/Cred_Data').getValue(2, 3))

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Email Id_ku-text-e'), var_email)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Email Id_ku-text-e'), 
    findTestData('Data Files/Credentials/Cred_Data').getValue(3, 3))

//Enter Password
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Password_ku-text-p'), var_password)
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Password_ku-text-p'), 
    findTestData('Data Files/Credentials/Cred_Data').getValue(4, 3))

//Select CheckBox 
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/input_Enter Password_ku-checkb'))

//Click on Submit to Login
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_RelatinshipManagerLogin/span_Submit'))

WebUI.delay(2)

//Click to Select the Lead
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectLead/span_SelectLead'))
String xpath = ('//span[contains(text(), "' + findTestData('Data Files/LeadGenerationAndAssign/Assigning').getValue(1, 1)) + 
'" )]'

TestObject to = new TestObject('path')

println(xpath)

to.addProperty('xpath', ConditionType.EQUALS, xpath)

WebUI.click(to)

WebUI.delay(1)

//Click on Loan Application Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/span_Loan Application Details'))

//Click on Select Repayment Frequency
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/divRepayfrq'))

WebUI.delay(2)

//Click on EMI option
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/div_Repayment'))
String xpath1 = ('//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(1, 1)) + '")]'

//TestObject to = new TestObject("path")
println(xpath1)

to.addProperty('xpath', ConditionType.EQUALS, xpath1)

WebUI.click(to)

WebUI.delay(2)

//Click on Select Security offered
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/divSecurityOffered'))

WebUI.delay(2)

//Click on option 
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/div_Automobile'))
String xpath2 = ('//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(2, 1)) + '")]'

//TestObject to = new TestObject("path")
to.addProperty('xpath', ConditionType.EQUALS, xpath2)

WebUI.click(to)

WebUI.delay(2)

//Click on Select ID Proof of the Enterprise
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/divIDProof'))

WebUI.delay(2)

//Click on option PAN
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/Tab_LoanApplicationDetails/div_PAN'))
String xpath3 = ('//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(3, 1)) + '")]'

//TestObject to = new TestObject("path")
to.addProperty('xpath', ConditionType.EQUALS, xpath3)

WebUI.click(to)

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_LoanApplicationDetails/span_Loan Application Details'))

WebUI.delay(1)

//Click on Business Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/span_Business Details'))

WebUI.delay(2)

//Click on Business Vintage
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/input__ku-text-businessVintage'), findTestData(
        'Data Files/Pre-qualAndPD1/Pre_qual').getValue(4, 1))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BusinessDetails/span_Business Details'))

WebUI.delay(1)

//Click on Personal Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/span_Personal Details'))

WebUI.delay(1)

//Send DoB
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/input__ku-datepicker-dob'))

WebUI.delay(2)

//Calender Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/Tab_Calender/div_2019'))

WebUI.delay(2)

//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/Tab_Calender/span_1973'))
String xpath4 = ('//span[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(5, 1)) + '")]'

//TestObject to = new TestObject("path")
println(xpath4)

to.addProperty('xpath', ConditionType.EQUALS, xpath4)

WebUI.click(to)

WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/Tab_Calender/span_27'))

WebUI.delay(2)

//Select Gender
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/divGender'))

WebUI.delay(2)

//Select as
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/div_SelectGender'))
String xpath5 = ('//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(6, 1)) + '")]'

//TestObject to = new TestObject("path")
to.addProperty('xpath', ConditionType.EQUALS, xpath5)

WebUI.click(to)

WebUI.delay(1)

//Choose Marital Status
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/divMaritalStatus'))

WebUI.delay(1)

//Select as
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/div_SelectStatus'))
String xpath6 = ('//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(7, 1)) + '")]'

//TestObject to = new TestObject("path")
to.addProperty('xpath', ConditionType.EQUALS, xpath6)

WebUI.click(to)

WebUI.delay(1)

//Choose Qualification
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/divEducation'))

WebUI.delay(1)

//Select as
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/div_SelectEducation'))

WebUI.delay(1)

//Pass the Languages known
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/input__ku-text-languagesKnown'), 'English')

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_PersonalDetails/span_Personal Details'))

WebUI.delay(1)

//Open ID Proof tab
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/span_ID-Proof'))

//Enter PAN Card Number
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input__ku-text-panApplicant'), findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(
        8, 1))

//Enter Aadhar Number
//WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input__ku-text-aadharNumber'), findTestData('Data Files/LeadGeneration/LeadGen_Data').getValue(9, 1))
WebUI.delay(2)

//Driving Licence ?
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/input_Yes_radio-group-undefine'))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_IDProof/span_ID-Proof'))

WebUI.delay(2)

//Open OwnerShip Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/span_Address and Ownership Det'))

//Enter the Residence Address 
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/input__ku-text-rAddressLine1'), 'Flat No 12, Saptshrungi Mata Appartment, Near Rose Bekary, Lamkhede Mala, Tarwala Nagar, Dindori Road, Panchvati, Nashik')

//Enter the Pincode
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/input_Please enter a Pincode f'), findTestData(
        'Data Files/Pre-qualAndPD1/Pre_qual').getValue(9, 1))

//Check the CheckBox
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/input_State_ku-checkbox-curren'))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_OwnerShipDetails/span_Address and Ownership Det'))

WebUI.delay(3)

//Open Bank Details 
WebUI.click(findTestObject('Object Repository/RelationshipManagerForPre-Qual/Tab_BankDetails/span_Banking Details'))

//Enter the Account Number
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_Bank Account Number_ku-t'), findTestData(
        'Data Files/Pre-qualAndPD1/Pre_qual').getValue(10, 1))

//Enter the IFSC code 
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_IFSC Code Branch Address'), findTestData(
        'Data Files/Pre-qualAndPD1/Pre_qual').getValue(11, 1))

WebUI.delay(1)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_IFSC Code Branch Address'))

WebUI.delay(2)

//Enter the Vintage of Bank Acccount
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/input_Vintage of the Bank Acco'), findTestData(
        'Data Files/Pre-qualAndPD1/Pre_qual').getValue(12, 1))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/RelationshipManagerForPre-Qual/Tab_BankDetails/span_Banking Details'))

WebUI.delay(1)

//Open Other Loan Details
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/span_Other Loan Details'))

//Any Other Loan
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input__radio-group-undefined'))
println(findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(13, 1) == 'yes')

if (findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(13, 1) == 'yes') {
    String xpath8 = '(//input[@name=\'radio-group-takenOtherLoan\'])[@value=\'yes\']'

    //TestObject to = new TestObject("path")
    to.addProperty('xpath', ConditionType.EQUALS, xpath8)

    WebUI.click(to)

    WebUI.delay(2)

    //Enter the Name of Lender
    WebUI.sendKeys(findTestObject('Object Repository/RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/textarea_Obligations (Current'), 
        'Agarwal')

    //Select Type of Loan
    WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/divLoanType'))

    WebUI.delay(1)

    //Choose as 
    WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/div_SelectLoanType'))

    WebUI.delay(1)

    //Select Collateral
    WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/divCollateral'))

    WebUI.delay(1)

    //Choose as
    WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/div_SelectCollateral'))

    WebUI.delay(1)

    //Amount of Loan Sanctioned
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_Obligations (Current Yea'), 
        '59000')

    //Loan Distributed
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_hhjh_ku-text-loanDisburs'), 
        '59000')

    //Rate of Inst
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_hhjh_ku-text-rateOfInter'), 
        '13')

    //Principal o/s
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_hhjh_ku-text-principalOs'), 
        '44023')

    //Original Tenor
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-originalTeno'), 
        '24')

    //No.of Instalments
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-instalmentsP'), 
        '7')

    //Balance Tenor
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-balanceTenor'), 
        '17')

    //Monthly Obligation
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-monthlyOblig'), 
        '2975')

    //Obligations Current year
    WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/Tab_BankDetails/TakenLoanDetails/input_knk_ku-text-obligationsC'), 
        '2975') //TestObject to = new TestObject("path")
} else {
    String xpath9 = '(//input[@name=\'radio-group-takenOtherLoan\'])[@value=\'no\']'

    to.addProperty('xpath', ConditionType.EQUALS, xpath9)

    WebUI.click(to)

    WebUI.delay(2)
}

//Comment
WebUI.sendKeys(findTestObject('RelationshipManagerForPre-Qual/ComntAndNext/textarea_Personal Feedback or'), 'Whatever you want')

//Process Next
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/ComntAndNext/span_Next'))

WebUI.delay(3)

//Proceed to PD1
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_ProceedToPD1/Page_Svakarma Finance/span_Next'))

WebUI.delay(3)

//Select PD1 Location
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/div_PD1Location'))

WebUI.delay(2)

//Choose the Location
//WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/div_Business'))
String xpath7 = ('//div[contains(text(),"' + findTestData('Data Files/Pre-qualAndPD1/Pre_qual').getValue(14, 1)) + '")]'

//TestObject to = new TestObject("path")
to.addProperty('xpath', ConditionType.EQUALS, xpath7)

WebUI.click(to)

WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/span_Next'))

WebUI.delay(3)

//Calling TestCase
//WebUI.callTestCase(findTestCase("Test Cases/HappyPath/PD1_HappyPath"), null)
//Close Browser
WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser('http://dev-lms.svakarma.net/login')

//Maximizing the window
WebUI.maximizeWindow()

WebUI.sendKeys(findTestObject('AssignForPD/SalesManagerLogin/Page_Lead Management/input__jss9'), "sales.manager.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('AssignForPD/SalesManagerLogin/Page_Lead Management/input__password'), "sales.manager.1")

//Click on Login
WebUI.click(findTestObject('AssignForPD/SalesManagerLogin/Page_Lead Management/input__jss9 jss12'))
Thread.sleep(3)

/*//In SalesManager Home Select the Lead
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_SelectLead/div_LeadNumber'))*/

String Id = "${dynamicId}"
String xpath = '//div[contains(text(),"' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)

Thread.sleep(3)

//Click on AssignPD
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/Page_Lead Management/span_Assign for PD'))

//Assign for pre-qualification
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/div_'))

//Select in DropDown
WebUI.click(findTestObject('AssignForPD/Page_Lead Management/li_relationship.manager.1dev.c'))
WebUI.delay(2)

//Assign for PD2
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/div_2'))
WebUI.delay(1)

//Select in DropDown
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/li_credit.rm.1dev.com'))
WebUI.delay(2)

//Click on confirm
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/span_Confirm'))
WebUI.delay(1)

//Click on sales.manager.1dev.com to Logout
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/span_sales.manager.1dev.com'))

//Click on Logout
WebUI.click(findTestObject('Object Repository/AssignForPD/Page_Lead Management/li_Logout'))

//Close Browser
WebUI.closeBrowser()

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser("http://dev-re.svakarma.net")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/input_Enter Email Id_ku-text-e'), "credit.rm.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/input_Enter Password_ku-text-p'), "credit.rm.1")

//Select CheckBox
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/input_This field is required_k'))

//Click on Submit to Login
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_Creditrm1Login/span_Submit'))
WebUI.delay(2)

//Click to Select the Lead
//WebUI.click(findTestObject('Credit_rm1for_PD2/SelectLead/span_SelectLead'))

String Id = "${dynamicId}"
String xpath = '//span[contains(text(),"' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(2)

//Person Accompanied
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PDDetails/input_Person who accompanied t'), "Someone")

//Name of Person met
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PDDetails/input__ku-text-nameOfPersonMet'), "Some")
WebUI.delay(2)

//Designation
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_Designation'))
WebUI.delay(2)

//Select as
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_Applicant'))
WebUI.delay(2)

//Queries
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_queries'))
WebUI.delay(2)

//Select as
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/div_Borrower Himself'))
WebUI.delay(2)

//Click on Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PDDetails/span_Next'))
WebUI.delay(2)

//LoanAmount Required
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__ku-text-loanAmountRequi'), Keys.chord(Keys.CONTROL,'a'))
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__ku-text-loanAmountRequi'), "10000")
WebUI.delay(2)

//Rate of Interest
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__ku-text-loanRoi'), Keys.chord(Keys.CONTROL,'a'))
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__ku-text-loanRoi'), "20")
WebUI.delay(2)

//Loan Tenure
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__ku-text-tenure'), Keys.chord(Keys.CONTROL,'a'))
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__ku-text-tenure'), "12")
WebUI.delay(2)

//New to credit
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/input__radio-group-undefined'))
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_LoanRequirement/span_Next'))
WebUI.delay(2)

//Name Matching
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/input__radio-group-undefined'))
WebUI.delay(2)

//Comment on Name Matching
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/input__ku-text-commentOnNameMi'), "Procced")
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails1/span_Next'))
WebUI.delay(2)

//Enter Email
WebUI.scrollToElement(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails2/input_Email Id_ku-text-emailId'), 2)
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails2/input_Email Id_ku-text-emailId'), "some.121@gmail.com")

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_PersonalDetails2/span_Next'))
WebUI.delay(2)

//Household Details
//No. of members in home
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-membersInHouseh'), "4")

//No. of Adults
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-adultMembers'), "4")

//No. of Children
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input__ku-text-childrenMembers'), "0")

//Relation
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_Relation'))
WebUI.delay(2)

//Select as
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_Parent'))
WebUI.delay(1)

//Occupation
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/textarea_Frequency_ku-text-occ'), "Retired")

//Age
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input_Frequency_ku-text-ageOut'), "78")

//Address
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/textarea_Frequency_ku-text-add'), "#24 jdfbvkjvb")

//Marital Status
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_MaritalStatus'))
WebUI.delay(2)

//Select as
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/div_Unmarried'))
WebUI.delay(1)

//Income
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input_Frequency_ku-text-income'), "100000")

//Expense
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/input_Frequency_ku-text-expens'), "10000")

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/span_Next'))
WebUI.delay(2)

//Nature of Expense
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/div_NatureofExpense'))
WebUI.delay(2)

//Select as
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/div_Living Expenses'))
WebUI.delay(2)

//Monthly Amount
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/input_Other Information_ku-tex'), "10000")

//Other information
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/textarea_Other Information_ku-'), "svhjdv")

//Comments on Household
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails2/textarea_Comments on Household'), "sdvjdb")

//After Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails/span_Next2'))
WebUI.delay(2)

//Years in Residense
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/input__ku-text-noOfYearsInResi'), "60")

//Ownership
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_OwnershipStatus'))
WebUI.delay(2)

//Select as
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_Family Owned'))
WebUI.delay(2)

//Type of Structure
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_TypeofStructure'))
WebUI.delay(2)

//Select as 
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/div_Flats and Bunglow'))
WebUI.delay(2)

//Comment
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/textarea_Comments on Residence'), "dkjbvkjsbv")
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_HouseholdDetails3/span_Next'))
WebUI.delay(3)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome/span_Next'))
WebUI.delay(2)

//Life style 
WebUI.click(findTestObject('null'))
WebUI.delay(1)

WebUI.click(findTestObject('null'))
WebUI.delay(1)

WebUI.click(findTestObject('null'))
WebUI.delay(1)

WebUI.click(findTestObject('null'))
WebUI.delay(1)

WebUI.click(findTestObject('null'))
WebUI.delay(1)

WebUI.click(findTestObject('null'))
WebUI.delay(2)

WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/div_WhiteGoods'))
WebUI.delay(2)

WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/div_Several'))
WebUI.delay(1)

//LIC
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/input_LIC_ku-text-lic'), "10000000")

//Jewellery
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/input_Jewellery (Approx. value'), "1000000")
WebUI.delay(1)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_OtherIncome2/span_Next'))
WebUI.delay(2)

//Comments on Documents
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_Documents/textarea_Final Comments on the'), "sdbvdshfbhdbv")

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_Documents/span_Next'))
WebUI.delay(2)

//Product1 units
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 1_ku-text-unitRe'), "100")
WebUI.delay(1)

//Product1 Rupees
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 1_ku-text-rupees'), "1000000")
WebUI.delay(1)

//Product2 units
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 2_ku-text-unitRe'), "120")
WebUI.delay(1)

//Product2 Rupees
WebUI.sendKeys(findTestObject('Credit_rm1for_PD2/Page_CashFlow/input_Product 2_ku-text-rupees'), "2000000")
WebUI.delay(1)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow/span_Next'))
WebUI.delay(2)

//Next2
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow2/span_Next'))
WebUI.delay(2)

//Next3
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow3/span_Next'))
WebUI.delay(2)

//Next4
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow4/span_Next'))
WebUI.delay(2)

//Next5
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow5/span_Next'))
WebUI.delay(2)

//Next6
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow6/span_Next'))
WebUI.delay(2)

//Next7
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow7/span_Next'))
WebUI.delay(2)

//Next8
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow8/span_Next'))
WebUI.delay(2)

//Next9
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow9/span_Next'))
WebUI.delay(2)

//Next10
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_CashFlow10/span_Next'))
WebUI.delay(2)

//SuccessScreen
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_SuccessScreen/span_Next'))
WebUI.delay(2)

//FinalApproval
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_FinalApproval/div_FinalApproval'))
WebUI.delay(2)

//Approve
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_FinalApproval/div_Approve'))
WebUI.delay(2)

//Next
WebUI.click(findTestObject('Credit_rm1for_PD2/Page_FinalApproval/span_Next'))
WebUI.delay(3)

//Close Browser
WebUI.closeBrowser()






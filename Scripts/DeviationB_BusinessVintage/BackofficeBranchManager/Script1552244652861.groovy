import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser("http://dev-bo.svakarma.net/auth/login")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('Object Repository/BackofficeBranchManager/Page_BackOfficeBranchManagerLogin/input_Enter User Name..._ku-te'), "branch.manager.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('Object Repository/BackofficeBranchManager/Page_BackOfficeBranchManagerLogin/input_Enter Password..._ku-tex'), "branch.manager.1")

//Click on Submit to Login
WebUI.click(findTestObject('Object Repository/BackofficeBranchManager/Page_BackOfficeBranchManagerLogin/span_Sign in'))
WebUI.delay(5)

//Click on Assigned
WebUI.click(findTestObject('BackofficeBranchManager/Page_ValidateSpanAndLead/span_Assigned'))
WebUI.delay(1)

//Check if application is Assigned
//WebUI.click(findTestObject('BackofficeBranchManager/SelectLead/td_SelectLead'), FailureHandling.CONTINUE_ON_FAILURE)

String Id = "${dynamicId}"
String xpath = '//td[contains(text(), "' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to, FailureHandling.CONTINUE_ON_FAILURE)
WebUI.delay(2)

//Click on Deviated
WebUI.click(findTestObject('BackofficeBranchManager/Page_ValidateSpanAndLead/span_Deviated'))
WebUI.delay(1)

//Check if application is Deviated
//WebUI.click(findTestObject('BackofficeBranchManager/SelectLead/td_SelectLead'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(to, FailureHandling.CONTINUE_ON_FAILURE)
WebUI.delay(3)

//Approve
WebUI.click(findTestObject('Object Repository/BackofficeBranchManager/Page_ValidateSpanAndLead/span_Approve'))
WebUI.delay(1)

//Comment
WebUI.sendKeys(findTestObject('Object Repository/BackofficeBranchManager/Page_ValidateSpanAndLead/Page_BackOffice  Login/textarea_Enter Your Comment...'), "Proceed")

//Submit
WebUI.click(findTestObject('Object Repository/BackofficeBranchManager/Page_ValidateSpanAndLead/Page_BackOffice  Login/span_Submit'))
WebUI.delay(2)

//Close
WebUI.click(findTestObject('Object Repository/BackofficeBranchManager/Page_ValidateSpanAndLead/Page_BackOffice  Login/span_Close'))

WebUI.delay(3)

//Close Browser
WebUI.closeBrowser()



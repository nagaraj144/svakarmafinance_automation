import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

WebUI.openBrowser(null)

WebUI.maximizeWindow(null)

'Login'
WebUI.navigateToUrl('http://dev-re.svakarma.net/')

//WebUI.sendKeys(findTestObject('Login/input_Enter Email Id_ku-text-e'), UserName)
WebUI.setText(findTestObject('Login/input_Enter Email Id_ku-text-e'), 'relationship.manager.1@dev.com')

//WebUI.sendKeys(findTestObject('Login/input_Enter Password_ku-text-p'), Password)
WebUI.sendKeys(findTestObject('Login/input_Enter Password_ku-text-p'), 'relationship.manager.1')

WebUI.click(findTestObject('Login/input_Enter Password_ku-checkb'))

WebUI.click(findTestObject('Login/span_Submit'))

WebUI.delay(1)

//Select Lead for PD1
//WebUI.click(findTestObject('PD1/NavigateToPD1/LoanID_1914000034'))

String Id = "${dynamicId}"
String xpath = '//span[contains(text(),"' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)

Thread.sleep(2)

WebUI.delay(1)

//Select PD1 Location
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/div_PD1Location'))

WebUI.delay(2)

//Choose the Location
WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/div_Business'))

WebUI.delay(2)

WebUI.click(findTestObject('RelationshipManagerForPre-Qual/Page_SelectPD1Location/span_Next'))

WebUI.delay(3)

//Filling PD Details Information
//WebUI.click(findTestObject('Object Repository/PD1/PD Details/div_PD Details'))

//WebUI.click(findTestObject('PD1/NavigateToPD1/PD1_Next'))
//WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/PD Details/NameOfPersonMet'), 'YXZ')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/DesignationOfPersonMet_Dropdown'))

WebUI.click(findTestObject('Object Repository/PD1/PD Details/DropdownValue_Applicant'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/Who were the queries answ_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/Page_Svakarma Finance/div_Relative'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/PD Details/PD Details_Next'))

WebUI.delay(2)

//Filling the Loan Requirements Tab
WebUI.scrollToElement(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'), 1)

WebUI.click(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/dropdown-schemeDescription'), Keys.chord(Keys.BACK_SPACE))

WebUI.click(findTestObject('PD1/LoanRequirements/ScehemeValue_WORKING CAPITAL MSME'))

WebUI.scrollToElement(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), 0)

WebUI.click(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-valueOfAsset'), '100000')

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-commentsOnSecurity'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-commentsOnSecurity'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/LoanRequirements/Text-commentsOnSecurity'), 'TEST')

WebUI.click(findTestObject('PD1/LoanRequirements/LoanRequirement_Next'))

//Filling the Personal Details Tab
///WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/input_Yes_radio-group-undefine - Copy'))
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/input__radio-group-undefined'))

WebUI.scrollToElement(findTestObject('Object Repository/PD1/Personal_Details/ID_Proof_CheckBox'), 0)

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/ID_Proof_CheckBox'))

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/Bureau_CheckBox'))

WebUI.click(findTestObject('Object Repository/PD1/Personal_Details/span_Next'))

WebUI.delay(1)

//Filling the Business Details Tab
//Address and Contact Details
WebUI.click(findTestObject('Object Repository/PD1/Business_Details/AddressAndContactDetails/span_Next'))

WebUI.delay(2)

//Business Details
WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/Datepicker-enterprises'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/Select_Date'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/DocumenToVerifyDate_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/DocumenToVerifyDate_Dropdown_Value'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/PD1/Business_Details/Business_Details/GrapicalAreaOfCoverage_Dropdown'), 
    0)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/GrapicalAreaOfCoverage_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/GrapicalAreaOfCoverage_Dropdown_Value'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/span_Next'))

WebUI.delay(2)

//Business Details - Customer
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Business Details_Customer/Text-productsService'), 
    Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Business Details_Customer/Text-productsService'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Business Details_Customer/Text-productsService'), 
    'TEST')

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/modeOfAcquiringNewCustomers_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/modeOfAcquiringNewCustomers_Dropdown_Value'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/span_Next'), 0)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), 
    0)

//WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/modeOfAcquiringNewCustomers_Dropdown_Value'), Keys.chord(Keys.ESCAPE))
WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text-percentageRepea'), '5')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/TypeOfCustomers_Dropdown'))

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/Page_Svakarma Finance/div_B2B'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Business_Details/clickOnBody'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'), 
    Keys.chord(Keys.ESCAPE))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'), 
    Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/Text_NameOfMajorCustomers'), 
    'Test')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerAddress'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerAddress'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerAddress'), 'TEMP')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerContact'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerContact'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerContact'), '9967845637')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_customerYearsRelationship'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_customerYearsRelationship'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_customerYearsRelationship'), '3')

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerSales'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerSales'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Text_CustomerSales'), '10')

WebUI.click(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Dropdown_CustomerPaymentTerms'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('PD1/Business_Details/Page_Svakarma Finance/Dropdown_CustomerPaymentTerms_Value'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Business_Details/Page_Svakarma Finance/span_Next'))

//Management & Operations
WebUI.delay(1)

// Management
WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Management_Tab'))

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/WhoIsRunningOffice_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/WhoIsRunningOffice_Dropdown_Value'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-yearsInSameBusi'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-yearsInSameBusi'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-yearsInSameBusi'), '10')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-peopleActivelyI'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-peopleActivelyI'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/input__ku-text-peopleActivelyI'), 'Basava M')

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Management_Tab'))

WebUI.delay(1)

// Operations
WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Operations_Tab'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/People_Employeed'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/People_Employeed'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/People_Employeed'), '100')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Employeees_seen'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Employeees_seen'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Employeees_seen'), '100')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Seasonality_Radio_Button'))

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/BillBook_Radio_Button'))

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Tool_Radio_Button'))

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Payment_Channel_Radio_Button'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Name'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Name'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Name'), 'XYZ')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/contanct_number_of_people'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/contanct_number_of_people'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/contanct_number_of_people'), '9876456789')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/comments'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/comments'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Management  Operations/Operations/comments'), 'TEST')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Refe_Check_Dropdown'))

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Ref_Check_Dropdown_Value'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/Management  Operations/Operations/Operations_Tab'))

WebUI.delay(1)

//Offices
WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_Tab'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfOffice_Dropdown'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfOffice_Dropdown_value'))

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_OwnerShip_Dropdown'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_OwnerShip_Dropdown_Value'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/yearsCurrentOffice'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/yearsCurrentOffice'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/yearsCurrentOffice'), '10')

WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfStructure_Dropdown'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/typeOfStructure_Dropdown_Value'))

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Name_Board_RadioButton'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/localityOfOffice_Dropdown'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/localityOfOffice_Dropdown_Value'))

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupShown_Dropdown'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupShown_Dropdown_Value'))

WebUI.delay(2)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupSuitability_Dropdown'))

WebUI.click(findTestObject('PD1/Management  Operations/Offices/businessSetupSuitability_Dropdown_Value'))

WebUI.delay(2)

not_run: WebUI.scrollToElement(findTestObject('PD1/Management  Operations/Offices/Office_RadioButton'), 0)

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_RadioButton'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('PD1/Management  Operations/Offices/HoursorDayInOffice'), '24/7')

WebUI.click(findTestObject('PD1/Management  Operations/Offices/Office_Next'))

WebUI.delay(1)

//Licenses
WebUI.click(findTestObject('Object Repository/PD1/Business Finance/ShopsAndEsta'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Gst_Cert'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/ExportImport_Cert'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/UdhyogAdhar'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/FoodSafety_Licence'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/FDA'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/PollutionControl'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Fire_Control'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/FactriesAct'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/LicenceCheck'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Business_Finance_Next'))

WebUI.delay(1)

//Business Fianance
//Finance Working Capital
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Debtors_cycle'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Debtors_cycle'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Debtors_cycle'), '15')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Ceditors_cycle'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Ceditors_cycle'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Finance-Working Capital/Ceditors_cycle'), '15')

WebUI.click(findTestObject('PD1/Business Finance/Finance-Working Capital/Next'))

WebUI.delay(1)

//Financials Revenue
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/turnoverAsPerBooks'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/turnoverAsPerBooks'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/turnoverAsPerBooks'), '10000000')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/operatingMarginAsPerBooks'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/operatingMarginAsPerBooks'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/operatingMarginAsPerBooks'), '30')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/netProfitAsPerBooks'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/netProfitAsPerBooks'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/netProfitAsPerBooks'), '2500')

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Turnovers'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Estimated_OM'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Estimated_NetProfit'))

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/Financial Revenue/Next'))

WebUI.delay(2)

//Banking Details
WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/ifscCode'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/ifscCode'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/ifscCode'), 'SBIN0008949')

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/bankName'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/beneficiaryName'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/beneficiaryName'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/beneficiaryName'), 'Basava')

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/bankAccountNumber'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/bankAccountNumber'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/bankAccountNumber'), '1098765432')

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Radio_Button'))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/vintageBankAccount'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/vintageBankAccount'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/vintageBankAccount'), '12')

WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))

WebUI.delay(1)

//Other Details
//Property Details
WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))

WebUI.delay(1)

//Existing Loans
WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))

WebUI.delay(1)

//Documents
//WebUI.scrollToElement(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))
WebUI.click(findTestObject('Object Repository/PD1/Business Finance/BankingDetails/Next'))

WebUI.delay(1)

//Cash Flow Details
//Financial Statements
WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Business_Cycle'))

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/FinancialStatement_Next'))

WebUI.delay(1)

//Financial Statement Computation
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Financial Statement Computation_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Financial Statement Computation_Next'))

WebUI.delay(1)

//Raw Material
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material_Next'))

WebUI.delay(1)

//Raw Material Computation
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material Computation_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Raw Material Computation_Next'))

WebUI.delay(1)

//Charges
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Charges_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Charges_Next'))

WebUI.delay(1)

//Charge Computation
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Charge Computation_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Charge Computation_Next'))

WebUI.delay(1)

//BalanceSheet
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet_Next'))

WebUI.delay(1)

//BalanceSheet2
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet2_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/BalanceSheet2_Next'))

WebUI.delay(1)

//Totals
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Totals_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Totals_Next'))

WebUI.delay(1)

//Cash Flow Model
WebUI.scrollToElement(findTestObject('Object Repository/PD1/CashFlowDetails/Cash Flow Model_Next'), 0)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Cash Flow Model_Next'))

WebUI.delay(1)

//Successs Screen
WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Successs Screen_Next'))

WebUI.delay(1)

//Decision
WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Decision_Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/DecisionDropdown_Value'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/PD1/CashFlowDetails/Decision_Next'))

WebUI.delay(1)

WebUI.closeBrowser();

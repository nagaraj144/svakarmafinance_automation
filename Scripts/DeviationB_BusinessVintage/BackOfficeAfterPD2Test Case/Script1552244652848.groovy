import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType

//Open Browser
WebUI.openBrowser("http://dev-bo.svakarma.net/auth/login")

//Maximizing the window
WebUI.maximizeWindow()

//Enter Email address
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD2/Page_BackOfficeAfterPD2Login/input_Enter User Name..._ku-te'), "credit.rm.1@dev.com")

//Enter Password
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD2/Page_BackOfficeAfterPD2Login/input_Enter Password..._ku-tex'), "credit.rm.1")

//Click on Submit to Login
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_BackOfficeAfterPD2Login/span_Sign in'))

WebUI.delay(3)

//Click to Select the Lead
//WebUI.click(findTestObject('BackOfficeAfterPD2/Page_SelectLead/td_SelectLead'))

String Id = "${dynamicId}"
String xpath = '//td[contains(text(), "' + Id + '")]'
TestObject to = new TestObject("path")
to.addProperty("xpath", ConditionType.EQUALS, xpath)
WebUI.click(to)
WebUI.delay(3)

//Select Cash flow Model
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/li_Cash Flow Model'))
WebUI.delay(8)

//Edit
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/span_EDIT'))
WebUI.delay(2)

//Select Security Cover
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_SecurityCover'))
WebUI.delay(2)

//Choose Medium
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_Medium'))
WebUI.delay(2)

//Select Compliance report
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_ComplianceReport'))
WebUI.delay(2)

//Choose Negative
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_Negative'))
WebUI.delay(2)

//Select BureauAnalysis
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_BureauAnalysis'))
WebUI.delay(2)

//Choose Above threshold
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_Above Threshold for all'))
WebUI.delay(2)

//Select Client Vintage
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_ClientVintage'))
WebUI.delay(2)

//Choose Greater
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_Greater than one year rela'))
WebUI.delay(2)

//Select Feedback on PD
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_FeedbackOnPD'))
WebUI.delay(2)

//Choose as Positive
WebUI.click(findTestObject('BackOfficeAfterPD2/Page_CasFlowModel/div_Positive'))
WebUI.delay(2)

//Select Name Mismatch
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_NameMismatch'))
WebUI.delay(2)

//Choose as No
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_No'))
WebUI.delay(2)

//Select DOB Mismatch
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_DOBMismatch'))
WebUI.delay(2)

//Choose as No
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_No'))
WebUI.delay(2)

//Select Address Mismatch
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_AddressMismatch'))
WebUI.delay(2)

//Choose as No
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_NoAdd'))
WebUI.delay(2)

//Select Other Deviation
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_OtherDeviations'))
WebUI.delay(2)

//Choose as No
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/div_NoOthDiv'))
WebUI.delay(3)

//Save the Changes
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/span_SAVE'))
WebUI.delay(5)

//Trigger credit
WebUI.click(findTestObject('BackOfficeAfterPD2/Page_CasFlowModel/span_Trigger Credit Model'))
WebUI.delay(3)

//close popup
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/span_Close'))
WebUI.delay(3)

//Select Business Details
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_EnterBusinessPAN/li_Business Details'))
WebUI.delay(2)

//Edit
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_EnterBusinessPAN/span_EDIT'))
WebUI.delay(2)

//Enter Business PAN
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD2/Page_EnterBusinessPAN/input_Business PAN_ku-text-bus'), "AAAPL1235C")
WebUI.delay(2)

//Save
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_EnterBusinessPAN/span_SAVE'))
WebUI.delay(6)

//Select CAM
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/li_CAM'))
WebUI.delay(3)

//Generate CAM
WebUI.scrollToElement(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/span_Generate CAM'), 3)
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/span_Generate CAM'))
WebUI.delay(15)

//Switching the tab
WebUI.switchToWindowIndex(0)
WebUI.delay(5)

//Approve
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/span_Approve'))
WebUI.delay(2)

//Enter comment
WebUI.sendKeys(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/Page_Dashboard/textarea_Enter Your Comment...'), "Process")
WebUI.delay(2)

//Submit
WebUI.click(findTestObject('Object Repository/BackOfficeAfterPD2/Page_CasFlowModel/Page_Dashboard/span_Submit'))
WebUI.delay(5)

//Close browser
WebUI.closeBrowser()








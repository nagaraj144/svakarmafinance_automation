<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>HappyFlow</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>3d4ab118-bd1d-4fb2-9da9-e04826246891</testSuiteGuid>
   <testCaseLink>
      <guid>80e4b72e-0414-4390-ae8c-a382cc15d55d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HappyPath/AssignForPDTestCase</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8adbc9ef-138f-41b5-84f5-c307e5d36794</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LeadGeneration/Assigning</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9f9b1d18-9c9b-418f-94de-dd2444044565</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HappyPath/RelationshipManagerForPre-QualTestCase</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>852519fd-5301-4d4f-9658-d8ed756d818c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LeadGeneration/LeadGen_Data</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1839357c-443a-4bd8-9c90-85fae0b08c9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/PD1_HappyPath</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f1c44502-be90-4cd0-9b64-8c73de716f73</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Pre-qual/Pre_qual</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>535ac0b2-de06-404c-994c-fd26a5876901</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/Continuation_of_PD1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>294017c9-6220-4d76-a865-9dbfb499bb84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/BackOfficeAfterPD1Test Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b79fc8-b23d-4359-8ee2-496fd32ca0c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/Creditrm1forPD2_Test Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9e4a00e-ca46-4eb0-8ad3-ec43477ff075</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/BackOfficeAfterPD2Test Case</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bf8e57df-b050-4b66-8e81-a486530330d9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e89d6405-2d70-4a36-83f2-4a834d634e86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/BackofficeBranchManager</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>261f144f-43a7-4b82-969f-79e3a1d83808</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1e28e403-bf49-4600-b99c-26a9dab9a3a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/BackofficeRiskManagerTest Case</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dd6b1033-a7b5-4028-a27e-17ee5a4f179f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c3c5a023-dda7-4c60-8d8e-08233ba4aa77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/BackofficeBusinessHeadTest Case</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>14f6f486-2ab8-42a4-b7a0-a74d6bdfa329</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cc781e40-4de0-4573-875b-e0e5683cb6f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HappyPath/CreditOperationsTest Case</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>051ba7ed-5d70-43d5-83d9-578302726bc4</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

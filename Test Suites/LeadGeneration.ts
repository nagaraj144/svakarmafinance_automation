<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LeadGeneration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>efbca0ca-dce6-483f-b7ec-2bcbd58f003b</testSuiteGuid>
   <testCaseLink>
      <guid>cca7258f-a470-44b1-9990-f3c2928eb6b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HappyPath/LeadGenerate</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e31da5cf-9776-4c68-a0da-3e296f5106bd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Credentials/Cred_Data</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0de6890b-361b-4c1f-a97b-ed848f44cbc2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LeadGeneration/LeadGen_Data</testDataId>
      </testDataLink>
   </testCaseLink>
</TestSuiteEntity>

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/HappyFlow')

suiteProperties.put('name', 'HappyFlow')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\Kuliza-269\\Documents\\Repo\\NewCode\\qa-automation\\Reports\\HappyFlow\\20190322_161132\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/HappyFlow', suiteProperties, [new TestCaseBinding('Test Cases/HappyPath/AssignForPDTestCase - Iteration 1', 'Test Cases/HappyPath/AssignForPDTestCase',  [ 'Application' : '1921000034' , 'AssignPD1' : 'relationship.manager.1@dev.com' , 'AssignPD2' : 'credit.rm.1@dev.com' ,  ]), new TestCaseBinding('Test Cases/HappyPath/AssignForPDTestCase - Iteration 2', 'Test Cases/HappyPath/AssignForPDTestCase',  [ 'Application' : '1921000035' , 'AssignPD1' : 'relationship.manager.1@dev.com' , 'AssignPD2' : 'credit.rm.1@dev.com' ,  ])])

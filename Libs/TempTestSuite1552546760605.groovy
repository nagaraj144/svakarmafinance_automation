import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Deviation_A')

suiteProperties.put('name', 'Deviation_A')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\Kuliza-367\\Desktop\\Katalon_Backup\\DemoTest\\Reports\\Deviation_A\\20190314_122920\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Deviation_A', suiteProperties, [new TestCaseBinding('Test Cases/DeviationA_DateOfBirth/BackOfficeAfterPD2Test Case', 'Test Cases/DeviationA_DateOfBirth/BackOfficeAfterPD2Test Case',  null), new TestCaseBinding('Test Cases/DeviationA_DateOfBirth/BackofficeBranchManager', 'Test Cases/DeviationA_DateOfBirth/BackofficeBranchManager',  null), new TestCaseBinding('Test Cases/DeviationA_DateOfBirth/BackofficeRiskManagerTest Case', 'Test Cases/DeviationA_DateOfBirth/BackofficeRiskManagerTest Case',  null), new TestCaseBinding('Test Cases/DeviationA_DateOfBirth/BackofficeBusinessHeadTest Case', 'Test Cases/DeviationA_DateOfBirth/BackofficeBusinessHeadTest Case',  null), new TestCaseBinding('Test Cases/DeviationA_DateOfBirth/CreditOperationsTest Case', 'Test Cases/DeviationA_DateOfBirth/CreditOperationsTest Case',  null)])

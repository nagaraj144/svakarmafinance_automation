import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/LeadGeneration')

suiteProperties.put('name', 'LeadGeneration')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\Kuliza-269\\Documents\\Repo\\NewCode\\qa-automation\\Reports\\LeadGeneration\\20190322_182056\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/LeadGeneration', suiteProperties, [new TestCaseBinding('Test Cases/HappyPath/LeadGenerate', 'Test Cases/HappyPath/LeadGenerate',  [ 'NOE' : 'TESTLEAD7' , 'BN' : '1' , 'NOB' : '1' , 'TOE' : '3' , 'Password' : 'relationship.manager.1' , 'AOLR' : '10000' , 'POL' : '2' , 'UserName' : 'relationship.manager.1@dev.com' , 'OP' : '440002' , 'MN' : '9976345023' , 'ROI' : '20' , 'SOURCE' : '2' , 'FB' : 'Basava' , 'LB' : 'T' , 'OA' : 'Maharastraa' , 'TENURE' : '14' , 'AT' : '100000' ,  ])])
